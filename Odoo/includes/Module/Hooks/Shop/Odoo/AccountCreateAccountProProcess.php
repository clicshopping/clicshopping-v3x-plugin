<?php
/*
   * AdditionalCheckoutButtons.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\Shop\Odoo;

  use ClicShopping\OM\Registry;

  class AccountCreateAccountProProcess {

    public function __construct()  {

      $this->customer_id = $_POST['customer_id'];
      $this->firstname = $_POST['firstname'];
      $this->lastname = $_POST['lastname'];
      $this->email_address  = $_POST['email_address'];
      $this->newsletter  =  $_POST['newsletter'];
      $this->telephone  =  $_POST['telephone'];
      $this->cellular_phone  =  $_POST['cellular_phone'];
      $this->fax  =  $_POST['fax'];
      $this->company_website  =  $_POST['company_website'];
      $this->street_address  =  $_POST['street_address'];
      $this->suburb  =  $_POST['suburb'];
      $this->postcode  =  $_POST['postcode'];
      $this->city  =  $_POST['city'];
    }


//************************************************
// Country Code
//************************************************

    private function getCountryIdCustomer() {
      $OSCOM_Db = Registry::get('Db');

      $QcountryIdCustomer = $OSCOM_Db->prepare("select entry_country_id
                                                 from :table_address_book
                                                 where customers_id = :customers_id
                                                ");
      $QcountryIdCustomer->bindInt(':customers_id',  $this->customer_id );
      $QcountryIdCustomer->execute();

      $country_id_customer = $QcountryIdCustomer->valueInt('entry_country_id');

      return $country_id_customer;
    }

    private function getCountryCode() {
      $OSCOM_Db = Registry::get('Db');

      $QcountryCode = $OSCOM_Db->prepare("select countries_iso_code_2
                                           from :table_countries
                                           where countries_id = :countries_id
                                          ");
      $QcountryCode->bindInt(':countries_id', $this->getCountryIdCustomer());
      $QcountryCode->execute();

      $country_code = $QcountryCode->valueInt('countries_iso_code_2');

      return $country_code;
    }

    private function getCountryIdOdoo() {
      Registry::set('Odoo',  new Odoo());
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('code', '=', $this->getCountryCode(), 'res.country');

      $field_list = array('id',
                          'name'
                        );

      $country_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.country');
      $country_id_odoo = $country_id_odoo[0][id];

      return $country_id_odoo;
    }



    public function save() {

      Registry::set('Odoo',  new Odoo());
      $OSCOM_ODOO = Registry::get('Odoo');

// label to include in Odoo
      $OSCOM_ODOO->getLabelName('Customer');
      $labelId = $OSCOM_ODOO->getLabelId();
      $type_string = $OSCOM_ODOO->getLabelType();

//country
      $OSCOM_ODOO->getCustomerId($this->customer_id);
      $CountryIdOdoo = $OSCOM_ODOO->getCountryIdOdoo();

      if ( $this->newsletter == 1)  $this->newsletter = 0;

      $values = array (
                        "ref" => new xmlrpcval('WebStore - ' . $this->customer_id, "string"),
                        "phone" => new xmlrpcval($this->telephone,"string"),
                        "mobile" => new xmlrpcval($this->cellular_phone,"string"),
                        "fax" => new xmlrpcval($this->fax,"string"),
                        "website" => new xmlrpcval($this->company_website,"string"),
                        "street" => new xmlrpcval($this->street_address,"string"),
                        "street2"=> new xmlrpcval($this->suburb,"string"),
                        "zip" => new xmlrpcval($this->postcode,"string"),
                        "city" => new xmlrpcval($this->city,"string"),
                        "comment" => new xmlrpcval('Website Registration - Customer B2B', "string"),
                        "name"    => new xmlrpcval( $this->lastname . ' ' . $this->firstname, "string"),
                        "email"  => new xmlrpcval( $this->email_address, "string"),
                        "country_id" => new xmlrpcval($CountryIdOdoo,"int"),
                        "tz"      => new xmlrpcval("Europe/Paris", "string"),
                        "opt_out"  => new xmlrpcval($this->newsletter, "double"),
                        'category_id' => new \xmlrpcval($labelId, $type_string),
                      );

      $OSCOM_ODOO->createOdoo($values, "res.partner");
    } // end save
  } //end class
