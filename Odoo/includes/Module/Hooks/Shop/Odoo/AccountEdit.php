<?php
/*
   * AdditionalCheckoutButtons.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\Shop\Odoo;

  use ClicShopping\OM\Registry;
  use ClicShopping\OM\OSCOM;

  class AccountEdit {

    public function __construct()  {
      $OSCOM_Customer = Registry::get('Customer');

      if (!$OSCOM_Customer->isLoggedOn()) {
        OSCOM::redirect('index.php', 'Account&LogIn', 'SSL');
      }

      $this->firstname = $_POST['firstname'];
      $this->lastname = $_POST['lastname'];
      $this->emailAddress  = $_POST['email_address'];
      $this->telephone  =  $_POST['telephone'];
      $this->cellularPhone  =  $_POST['cellular_phone'];
      $this->fax  =  $_POST['fax'];
    }


    public function save() {

      $OSCOM_Customer = Registry::get('Customer');
      $OSCOM_ODOO = Registry::get('Odoo');

// **********************************
// Search odoo customer id
// **********************************
      $ids = $OSCOM_ODOO->odooSearch('clicshopping_customers_id', '=', $OSCOM_Customer->getID(), 'res.partner');

// **********************************
// read id customer odoo
// **********************************
      $field_list = array('id');

      $id_odoo_customer_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $id_odoo_customer = $id_odoo_customer_array[0][id];

// **********************************
// Update data
// **********************************
      $values = array (
                        "name"    => new \xmlrpcval( $this->lastname . ' ' . $this->firstname, "string"),
                        "email"  => new \xmlrpcval($this->emailAddress, "string"),
                        'phone' => new \xmlrpcval($this->telephone,"string"),
                        'mobile' => new \xmlrpcval($this->cellularPhone,"string"),
                        'fax' => new \xmlrpcval($this->fax,"string"),
                      );

      $OSCOM_ODOO->updateOdoo($id_odoo_customer, $values, "res.partner");
    } // end save
  } //end class
