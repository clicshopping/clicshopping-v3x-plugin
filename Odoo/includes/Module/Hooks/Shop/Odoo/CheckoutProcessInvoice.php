<?php
/*
 * CheckoutProcessInvoice.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\Shop\Odoo;

  use ClicShopping\OM\Registry;
  use ClicShopping\OM\DateTime;
  use ClicShopping\Sites\Shop\HTMLOverride;

  class CheckoutProcessInvoice {

    public function __construct()  {
      
      $OSCOM_Db = Registry::get('Db');
      $OSCOM_Customer = Registry::get('Customer');
      $OSCOM_ODOO = Registry::get('Odoo');

      if (!$OSCOM_Customer->isLoggedOn()) {
        OSCOM::redirect('index.php', 'Account&LogIn', 'SSL');
      }

      $Qcustomers = $OSCOM_Db->prepare('select customers_id,
                                                orders_id,
                                                orders_status,
                                                date_purchased,
                                                orders_status_invoice,
                                                orders_date_finished,
                                                odoo_invoice,
                                                delivery_postcode,
                                                billing_postcode,
                                                provider_name_client,
                                                client_computer_ip,
                                                customers_group_id,
                                                customers_siret,
                                                customers_ape,
                                                customers_tva_intracom,
                                                customers_company,
                                                payment_method
                                           from :table_orders
                                           where orders_id = :orders_id
                                         ');
      $Qcustomers->bindValue(':orders_id', $this->getId());
      $Qcustomers->execute();

      $this->customersId = $OSCOM_Customer->getID();
      $this->orderId = $Qcustomers->valueInt('orders_id');
      $this->dateInvoicePurchased = $Qcustomers->value('date_purchased');
      $this->ordersStatusInvoice = $Qcustomers->valueInt('orders_status_invoice');
      $this->odooInvoice = $Qcustomers->valueInt('odoo_invoice');
      $this->deliveryPostcode =  $Qcustomers->value('delivery_postcode');
      $this->billingPostcode = $Qcustomers->value('billing_postcode');
      $this->providerNameClient = $Qcustomers->value('provider_name_client');
      $this->clientComputerIp = $Qcustomers->value('client_computer_ip');
      $this->customers_group_id = $OSCOM_Customer->getCustomersGroupID();
      $this->customersSiret = $Qcustomers->value('customers_siret');
      $this->customersApe = $Qcustomers->value('customers_ape');
      $this->customersTvaIntracom = $Qcustomers->value('customers_tva_intracom');
      $this->customersCompany = $Qcustomers->value('customers_company');
      $this->paymentMethod = $Qcustomers->value('paymentMethod');
      $this->refWebstore = DateTime::getDateReferenceShort($this->dateInvoicePurchased) . 'S';
      $this->companyId = $OSCOM_ODOO->getSearchCompanyIdOdoo();

      $this->sendTo = $_SESSION['sendto'];
      $this->billTo = $_SESSION['billto'];

      $this->ordersStatusInvoice = $Qcustomers->valueInt('orders_status_invoice');
    }

    private function getId() {
      if (isset($_POST['insert_id']) && is_numeric($_POST['insert_id']) ) {
        $insert_id = $_POST['insert_id'];
      }

      return $insert_id;
    }

    private function getPartnerId() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_customers_id', '=', $this->customersId, 'res.partner');

      $field_list = array('id');

      $partner_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $partner_id = $partner_id[0][id];

      return $partner_id;
    }

    private function getPartnerInvoiceId() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_customers_additional_address_id', '=', $this->billTo, 'res.partner');

      $field_list = array('ref',
                          'id'
                          );

      $partner_invoice_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $partner_invoice_id = $partner_invoice_id[0][id];


      return $partner_invoice_id;
    }

    private function getPartnerShippingId() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_customers_additional_address_id', '=', $this->sendTo, 'res.partner');

      $field_list = array('id');

      $partner_shipping_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $partner_shipping_id = $partner_shipping_id[0][id];

      return $partner_shipping_id;
    }


    private function getInvoiceId() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_invoice_reference', '=', $this->refWebstore . $this->getId(), 'account.invoice');

      $field_list = array('id');

      $invoice_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'sale.order');
      $invoice_id = $invoice_id[0][id];

      return $invoice_id;
    }


    private function getAccountIdAccountSell() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('code', '=',ODOO_WEB_SERVICE_ACCOUNT_SELL, 'account.account');

      $field_list = array('id');

      $Qaccount_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'account.account');
      $account_id = $Qaccount_id[0][id];

      return $account_id;
    }


    private function getAccountIdAccountPurchase() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('code', '=', ODOO_WEB_SERVICE_ACCOUNT_PURCHASE, 'account.account');

      $field_list = array('id');

      $Qaccount_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'account.account');
      $account_id = $Qaccount_id[0][id];


      return $account_id;
    }


    public function save() {

      $OSCOM_Db = Registry::get('Db');
      $OSCOM_ODOO = Registry::get('Odoo');
      $OSCOM_Order = Registry::get('Order');

      $shipping_amount = $OSCOM_ODOO->getShippingAmount($this->getId());

      if ($this->odooInvoice == 0) {
// Create top invoice
        $values = array ("partner_id" => new \xmlrpcval($this->getPartnerId(), "int"),
                        "date_invoice" => new \xmlrpcval(date('Y-m-d'), "string"),
                        "company_id" => new \xmlrpcval($this->companyId, "int"),
                        "origin" => new \xmlrpcval("WebStore - order : " . $this->refWebstore . $this->getId(), "string"),
                        "account_id" => new \xmlrpcval($this->getAccountIdAccountPurchase(), "int"),
                        "clicshopping_invoice_id" => new \xmlrpcval( $this->orderId, "int"),
                        "clicshopping_invoice_reference" => new \xmlrpcval($this->refWebstore . $this->getId(), "string"),
                        "clicshopping_invoice_customer_id" => new \xmlrpcval($this->customersId, "int"),
                        "clicshopping_invoice_provider_name_client" => new \xmlrpcval($this->providerNameClient, "string"),
                        "clicshopping_invoice_client_computer_ip" => new \xmlrpcval($this->clientComputerIp, "string"),
                        "clicshopping_invoice_status" => new \xmlrpcval('instance', "string"),
                        "clicshopping_invoice_customers_group_id" => new \xmlrpcval($this->customers_group_id, "int"),
                        "clicshopping_invoice_customer_comments" => new \xmlrpcval($OSCOM_Order->info['comments'], "string"),
                      );

        $OSCOM_ODOO->createOdoo($values, "account.invoice");


// count number of product
        $count_products = sizeof($OSCOM_Order->products);

        for ($o=0, $n=$count_products; $o<$n; $o++) {

//******************************************
// research invoice_id by origin invoice
//***************************************
// ====> mettre en relation avec la B2B
        $ids = $OSCOM_ODOO->odooSearch('clicshopping_products_id', '=', $OSCOM_Order->products[$o]['id'], 'product.template');

// **********************************
// read id products odoo
// **********************************

        $field_list = array('id');

        $Qodoo_products_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
        $odoo_products_id = $Qodoo_products_id[0][id];

// **********************************
// name of products and options description
// doesn't take more than one options
// text limited  by odoo
// **********************************

          $products_name = $OSCOM_Order->products[$o]['name'];

          if (isset($OSCOM_Order->products[$o]['attributes']) && (sizeof($OSCOM_Order->products[$o]['attributes']) > 0)) {
            for ($j = 0, $k = sizeof($OSCOM_Order->products[$o]['attributes']); $j < $k; $j++) {
// attributes reference
              if ($OSCOM_Order->products[$o]['attributes'][$j]['reference'] != '' or $OSCOM_Order->products[$o]['attributes'][$j]['reference'] !='null') {
                $attributes_reference = $OSCOM_Order->products[$o]['attributes'][$j]['reference'] . ' - ';
              }
              $products_attributes = ' ' . $attributes_reference  . $OSCOM_Order->products[$o]['attributes'][$j]['option'] . ': ' . $OSCOM_Order->products[$o]['attributes'][$j]['value'];
              $products_attributes = HTML::HTMLOverride($products_attributes);
            }
          }

          if ($products_attributes != '') {
            $products_attributes = ' - ' . $products_attributes;
          }

          $products_name_odoo = $products_name . $products_attributes;

// **********************************
// tax
// **********************************

// select the good tax in function the products
          $QProductsTax = $OSCOM_Db->prepare('select products_tax
                                               from :table_orders_products
                                               where orders_id = :orders_id
                                               and products_id = :products_id
                                             ');
          $QProductsTax->bindInt(':orders_id', (int)$this->getId());
          $QProductsTax->bindValue(':products_id', $OSCOM_Order->products[$o]['id']);
          $QProductsTax->execute();

// select the tax code_tax_odoo in function the products tax
          $QProductsCodeTax = $OSCOM_Db->prepare('select code_tax_odoo
                                                   from :table_tax_rates
                                                   where tax_rate = :tax_rate
                                                 ');

          $QProductsCodeTax->bindValue(':tax_rate', $QProductsTax->value('products_tax'));
          $QProductsCodeTax->execute();

          $products_code_tax = $QProductsCodeTax->value('code_tax_odoo');

// research id tax by description
          if($products_code_tax != null) {
            $ids = $OSCOM_ODOO->odooSearch('description', '=', $products_code_tax, 'account.tax', 'string');
            $odoo_products_tax_id = $ids;
            $odoo_products_tax_id = $odoo_products_tax_id[0];

            if (!empty($odoo_products_tax_id)) {

              $type_tax_string = 'array';

              $tax = array(new \xmlrpcval(
                                          array(
                                                  new \xmlrpcval(6, "int"),// 6 : id link
                                                  new \xmlrpcval(0, "int"),
                                                  new \xmlrpcval(array(new \xmlrpcval($odoo_products_tax_id, "int")), "array")
                                                ), "array"
                                        )
                          );
            } else  {
              $tax = 0;
              $type_tax_string = 'int';
            }
          } else {
            $tax = 0;
            $type_tax_string = 'int';
          }
// Write a new line concerning the invoice
          $values = array (
                            "invoice_id" => new \xmlrpcval( $this->getInvoiceId(), "int"),
                            "account_id" => new \xmlrpcval( $this->getAccountIdAccountSell(), "int"),
                            "product_id" => new \xmlrpcval($odoo_products_id, "int"),
                            "company_id" => new \xmlrpcval($this->companyId, "int"),
                            "name" => new \xmlrpcval($products_name_odoo, "string"),
                            "price_unit" => new \xmlrpcval($OSCOM_Order->products[$o]['final_price'],"double"),
                            "quantity" => new \xmlrpcval($OSCOM_Order->products[$o]['qty'],'double'),
                            "invoice_line_tax_id" =>  new \xmlrpcval($tax, $type_tax_string),
                          );

          $OSCOM_ODOO->createOdoo($values, "account.invoice.line");
        } //end for

// Write a new line concerning the shipping by the service line available in odoo
        if ($OSCOM_ODOO->getShippingAccountingOdooId() != 0  || !is_null($OSCOM_ODOO->getShippingAccountingOdooId())) {

          if (DISPLAY_DOUBLE_TAXE =='true') {
// select the good tax in function the products
            $QProductsTax = $OSCOM_Db->prepare('select products_tax
                                                   from :table_orders_products
                                                   where orders_id = :orders_id
                                                   and products_id = :products_id
                                                 ');
            $QProductsTax->bindInt(':orders_id', (int)$this->getId());
            $QProductsTax->bindValue(':products_id', $OSCOM_Order->products[$o]['id']);
            $QProductsTax->execute();

            if  ( $QProductsTax->value('products_tax') != '' || $QProductsTax->value('products_tax') != 0) {
// select the tax code_tax_odoo in function the products tax
              $QProductsCodeTax = $OSCOM_Db->prepare('select code_tax_odoo
                                                       from :table_tax_rates
                                                       where tax_rate = :tax_rate
                                                     ');

              $QProductsCodeTax->bindValue(':tax_rate', $QProductsTax->value('products_tax'));

              $QProductsCodeTax->execute();

              $products_code_tax = $QProductsCodeTax->value('code_tax_odoo');


              if($products_code_tax != null) {

                $ids = $OSCOM_ODOO->odooSearch('description', '=', $products_code_tax, 'account.tax', 'string');
                $odoo_products_tax_id = $ids;
                $odoo_products_tax_id = $odoo_products_tax_id[0];

                if (!empty($odoo_products_tax_id)) {

                  $type_tax_string = 'array';

                  $tax = array(new \xmlrpcval(
                                              array(
                                                      new \xmlrpcval(6, "int"),// 6 : id link
                                                      new \xmlrpcval(0, "int"),
                                                      new \xmlrpcval(array(new \xmlrpcval($odoo_products_tax_id, "int")), "array")
                                                    ), "array"
                                            )
                              );
                } else  {
                  $tax = 0;
                  $type_tax_string = 'int';
                }
              } else {
                $tax = 0;
                $type_tax_string = 'int';
              }
            }
          } else {
            $tax = 0;
            $type_tax_string = 'int';
          }

          $values = array (
                            "invoice_id" => new \xmlrpcval( $this->getInvoiceId(), "int"),
                            "account_id" => new \xmlrpcval($OSCOM_ODOO->getShippingAccountingOdooId(), "int"),
                            "company_id" => new \xmlrpcval($this->companyId, "int"),
                            "product_id" => new \xmlrpcval(0, "int"),
                            "name" => new \xmlrpcval($OSCOM_ODOO->getShippingTitle($this->getId()), "string"),
                            "price_unit" => new \xmlrpcval($OSCOM_ODOO->getShippingAmount($this->getId()),"double"),
                            "quantity" => new \xmlrpcval(1,'int'),
                            "invoice_line_tax_id" =>  new \xmlrpcval($tax, $type_tax_string),
                          );

          $OSCOM_ODOO->createOdoo($values, "account.invoice.line");
        }

// Write a new line concerning the customer discount  by the service line available in odoo
        if ($OSCOM_ODOO->getCustomerDiscountAmount($this->getId()) != 0 || !is_null($OSCOM_ODOO->getCustomerDiscountAmount())) {
          $values = array (
                            "invoice_id" => new \xmlrpcval( $this->getInvoiceId(), "int"),
                            "company_id" => new \xmlrpcval($this->companyId, "int"),
                            "account_id" => new \xmlrpcval($OSCOM_ODOO->getDiscountAccountingOdooId(), "int"),
                            "product_id" => new \xmlrpcval(0, "int"),
                            "name" => new \xmlrpcval($OSCOM_ODOO->getCustomerDiscountTitle($this->getId()), "string"),
                            "price_unit" => new \xmlrpcval($OSCOM_ODOO->getCustomerDiscountAmount($this->getId()),"double"),
                            "quantity" => new \xmlrpcval(1,'int'),
                            "invoice_line_tax_id" =>  new \xmlrpcval(0, 'int'),
                          );
          $OSCOM_ODOO->createOdoo($values, "account.invoice.line");
        }


// Write a new line concerning the discount coupon by the service line available in odoo
        if ($OSCOM_ODOO->getDiscountCouponAmount($this->getId()) != 0 || !is_null($OSCOM_ODOO->getDiscountCouponAmount())) {
          $values = array (
                            "invoice_id" => new \xmlrpcval( $this->getInvoiceId(), "int"),
                            "company_id" => new \xmlrpcval($this->companyId, "int"),
                            "account_id" => new \xmlrpcval($OSCOM_ODOO->getDiscountAccountingOdooId(), "int"),
                            "product_id" => new \xmlrpcval(0, "int"),
                            "name" => new \xmlrpcval($OSCOM_ODOO->getDiscountCouponTitle($this->getId()), "string"),
                            "price_unit" => new \xmlrpcval($OSCOM_ODOO->getDiscountCouponAmount($this->getId()),"double"),
                            "quantity" => new \xmlrpcval(1,'int'),
                            "invoice_line_tax_id" =>  new \xmlrpcval(0, 'int'),
                          );

          $OSCOM_ODOO->createOdoo($values, "account.invoice.line");
        }

        $OSCOM_ODOO->buttonClickOdoo('account.invoice', 'button_reset_taxes',  $this->getInvoiceId());


        if ( $this->getInvoiceId() != '') {
// Update ClicShopping
          $Qupdate = $OSCOM_Db->prepare('update :table_orders
                                          set odoo_invoice = :odoo_invoice
                                          where orders_id = :orders_id
                                        ');
          $Qupdate->bindInt(':orders_id', $this->getId());
          $Qupdate->bindValue(':odoo_invoice', '2');
          $Qupdate->execute();
        }
      } // end $this->odooInvoice
    } // end save
  } //end class
