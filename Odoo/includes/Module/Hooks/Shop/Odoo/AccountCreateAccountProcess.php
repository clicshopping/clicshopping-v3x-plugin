<?php
/*
   * AdditionalCheckoutButtons.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\Shop\Odoo;

  use ClicShopping\OM\Registry;
  use ClicShopping\OM\OSCOM;

  class AccountCreateAccountProcess {

    public function __construct()  {
      $OSCOM_Customer = Registry::get('Customer');

      if (!$OSCOM_Customer->isLoggedOn()) {
        OSCOM::redirect('index.php', 'Account&LogIn', 'SSL');
      }

      $this->customerId = $OSCOM_Customer->getId();
      $this->firstname = $_POST['firstname'];
      $this->lastname = $_POST['lastname'];
      $this->email_address  = $_POST['email_address'];
      $this->newsletter  =  $_POST['newsletter'];
    }

    public function save() {

      $OSCOM_ODOO = Registry::get('Odoo');

// label to include in Odoo
      $OSCOM_ODOO->getLabelName('Customer');
      $labelId = $OSCOM_ODOO->getLabelId();
      $type_string = $OSCOM_ODOO->getLabelType();

// update newsletter
      if ($this->newsletter == 1) $this->newsletter = 0;

// **********************************
// write data
// **********************************
      $values = array ( "ref" => new \xmlrpcval('WebStore - ' . $this->customerId, "string"),
                        "comment" => new \xmlrpcval('Website Registration - Customer B2C', "string"),
                        "name"    => new \xmlrpcval( $this->lastname . ' ' . $this->firstname, "string"),
                        "email"  => new \xmlrpcval($this->email_address, "string"),
                        "tz"      => new \xmlrpcval("Europe/Paris", "string"),
                        "opt_out"  => new \xmlrpcval($this->newsletter, "double"),
                        "clicshopping_customers_id" => new \xmlrpcval($this->customerId, "int"),
                        'category_id' => new \xmlrpcval($labelId, $type_string),
                      );

      $OSCOM_ODOO->createOdoo($values, "res.partner");
    } // end save
  } //end class
