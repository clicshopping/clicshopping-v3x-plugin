<?php
/*
   * AdditionalCheckoutButtons.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\Shop\Odoo;

  use ClicShopping\OM\HTML;
  use ClicShopping\OM\Registry;
  use ClicShopping\OM\OSCOM;

  class AccountAddressBookProcess {

    public function __construct() {
      $OSCOM_Customer = Registry::get('Customer');

      if (!$OSCOM_Customer->isLoggedOn()) {
        OSCOM::redirect('index.php', 'Account&LogIn', 'SSL');
      }

      $this->odooCreate = $_POST['odoo_create'];
      $this->lastname = HTML::sanitize($_POST['lastname']);
      $this->firstname = HTML::sanitize($_POST['firstname']);
      $this->streetAddress = HTML::sanitize($_POST['street_address']);
      $this->suburb = HTML::sanitize($_POST['suburb']);
      $this->city = HTML::sanitize($_POST['city']);
      $this->postcode = HTML::sanitize($_POST['postcode']);
      $this->telephone = HTML::sanitize($_POST['telephone']);
      $this->cellularPhone = HTML::sanitize($_POST['cellular_phone']);
      $this->fax = HTML::sanitize($_POST['fax']);
      $this->newAddressBookId = HTML::sanitize($_POST['new_address_book_id']);
      $this->newcustomer = $_GET['newcustomer'];
      $this->shopping = $_POST['shopping'];
      $this->edit = $_GET['edit'];
    }

//************************************************
// Country Code
//************************************************
/*
    private function getCountryIdCustomer() {
      $OSCOM_Db = Registry::get('Db');
      $OSCOM_Customer = Registry::get('Customer');

      $QcountryIdCustomer = $OSCOM_Db->prepare("select entry_country_id
                                                 from :table_address_book
                                                 where customers_id = :customers_id
                                                ");
      $QcountryIdCustomer->bindInt(':customers_id', $OSCOM_Customer->getID() );
      $QcountryIdCustomer->execute();

      $country_id_customer = $QcountryIdCustomer->valueInt('entry_country_id');

      return $country_id_customer;
    }

    private function getCountryCode() {
      $OSCOM_Db = Registry::get('Db');

      $QcountryCode = $OSCOM_Db->prepare("select countries_iso_code_2
                                           from :table_countries
                                           where countries_id = :countries_id
                                          ");
      $QcountryCode->bindInt(':countries_id', $this->getCountryIdCustomer());
      $QcountryCode->execute();

      $country_code = $QcountryCode->value('countries_iso_code_2');

      return $country_code;
    }

    private function getCountryIdOdoo() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('code', '=', $this->getCountryCode(), 'res.country');

      $field_list = array('id',
                          'name'
                        );

      $country_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.country');
      $country_id_odoo = $country_id_odoo[0][id];

      return $country_id_odoo;
    }
*/




    private function getIdOdooCustomer() {
      $OSCOM_Customer = Registry::get('Customer');
      $OSCOM_ODOO = Registry::get('Odoo');

      if ($OSCOM_Customer->getDefaultAddressID() !=  $this->edit) {
        $search = $this->edit;
      } else {
        $search = $OSCOM_Customer->getId();
      }

//      $ids = $OSCOM_ODOO->odooSearch('clicshopping_customers_id', '=', $OSCOM_Customer->getID(), 'res.partner');
      $ids = $OSCOM_ODOO->odooSearch('ref', '=', 'WebStore - ' . $search, 'res.partner');

      $field_list = array('ref',
                          'id'
                        );

      $id_odoo_customer_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $id_odoo_customer = $id_odoo_customer_array[0][id];

      return $id_odoo_customer;
    }


    public function save() {

      $OSCOM_Customer = Registry::get('Customer');
      $OSCOM_ODOO = Registry::get('Odoo');

// label to include in Odoo
      $OSCOM_ODOO->getLabelName('Customer');
      $labelId = $OSCOM_ODOO->getLabelId();
      $type_string = $OSCOM_ODOO->getLabelType();

//country
      $OSCOM_ODOO->getCustomerId($OSCOM_Customer->getID());
      $OdooCustomerId = $OSCOM_ODOO->getCountryIdCustomer();
      $CountryIdOdoo = $OSCOM_ODOO->getCountryIdOdoo();


      if (isset($this->newcustomer) && isset($this->shopping)) {

          $values = array(
                            "name"    => new \xmlrpcval( $this->lastname . ' ' . $this->firstname, "string"),
                            'street'=>new \xmlrpcval($this->streetAddress,"string"),
                            'street2'=>new \xmlrpcval($this->suburb,"string"),
                            'city'=>new \xmlrpcval($this->city,"string"),
                            'zip'=>new \xmlrpcval($this->postcode,"string"),
                            'country_id'=>new \xmlrpcval($CountryIdOdoo,"int"),
                            'phone' => new \xmlrpcval($this->telephone,"string"),
                            'mobile' => new \xmlrpcval($this->cellularPhone,"string"),
                            'fax' => new \xmlrpcval($this->fax,"string"),
                            'category_id' => new \xmlrpcval($labelId, $type_string),
                          );

          $OSCOM_ODOO->updateOdoo($OdooCustomerId, $values, "res.partner");


        } elseif ($OdooCustomerId != '' && $this->odooCreate == false) {

          $values = array(
                          "name"    => new \xmlrpcval( $this->lastname . ' ' . $this->firstname, "string"),
                          'street'=>new \xmlrpcval($this->streetAddress,"string"),
                          'street2'=>new \xmlrpcval($this->suburb,"string"),
                          'city'=>new \xmlrpcval($this->city,"string"),
                          'zip'=>new \xmlrpcval($this->postcode,"string"),
                          'country_id'=>new \xmlrpcval($CountryIdOdoo,"int"),
                        );

          $OSCOM_ODOO->updateOdoo($OdooCustomerId, $values, "res.partner");

        } else {

          $values = array(
                          "name"    => new \xmlrpcval( $this->lastname . ' ' . $this->firstname, "string"),
                          'street'=>new \xmlrpcval($this->streetAddress,"string"),
                          'street2'=>new \xmlrpcval($this->suburb,"string"),
                          'city'=>new \xmlrpcval($this->city,"string"),
                          'zip'=>new \xmlrpcval($this->postcode,"string"),
                          'country_id'=>new \xmlrpcval($CountryIdOdoo,"int"),
                          "comment" => new \xmlrpcval('Web store New address book creation - relation with customer_id : ' . $OSCOM_Customer->getID() , "string"),
                          "ref" => new \xmlrpcval('WebStore New address - ' . $this->newAddressBookId, "string"),
                          'phone' => new \xmlrpcval($this->telephone,"string"),
                          'mobile' => new \xmlrpcval($this->cellularPhone,"string"),
                          'fax' => new \xmlrpcval($this->fax,"string"),
                          'category_id' => new \xmlrpcval($labelId, $type_string),
                        );

          $OSCOM_ODOO->createOdoo( $values, "res.partner");
        }
    } // end save
  } //end class
