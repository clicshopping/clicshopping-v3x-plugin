<?php
/*
   * AccountShippingAddress.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\Shop\Odoo;

  use ClicShopping\OM\Registry;
  use ClicShopping\OM\OSCOM;

  class AccountShippingAddress {

    public function __construct()  {

      $OSCOM_Customer = Registry::get('Customer');
      $OSCOM_Db = Registry::get('Db');

      if (!$OSCOM_Customer->isLoggedOn()) {
        OSCOM::redirect('index.php', 'Account&LogIn', 'SSL');
      }

      $QnewAddress = $OSCOM_Db->prepare('select entry_lastname,
                                                entry_firstname,
                                                entry_street_address,
                                                entry_suburb,
                                                entry_city,
                                                entry_postcode,
                                                entry_telephone,
                                                entry_country_id
                                       from :table_address_book
                                       where address_book_id = :address_book_id
                                       and customers_id = :customers_id
                                     ');
      $QnewAddress->bindInt(':address_book_id', (int)$_SESSION['sendto'] );
      $QnewAddress->bindInt(':customers_id', $OSCOM_Customer->getID());
      $QnewAddress->execute();
      $new_address = $QnewAddress->fetch();

      $this->lastname  = $new_address['entry_lastname'];
      $this->firstname = $new_address['entry_firstname'];
      $this->entryStreetAddress = $new_address['entry_street_address'];
      $this->entrySuburb = $new_address['entry_suburb'];
      $this->entryCity = $new_address['entry_city'];
      $this->entryPostcode = $new_address['entry_postcode'];
      $this->entryTelephone = $new_address['entry_telephone'];
      $this->addressBookCountryID = $new_address['entry_country_id'];
      $this->odooCreateShippingAddress =  $_POST['odoo_create_shipping_address'];
      $this->sendTo = $_SESSION['sendto'];
    }

    private function getIdOdooPartner() {
      $OSCOM_Customer = Registry::get('Customer');
      $OSCOM_ODOO = Registry::get('Odoo');

      if  ($this->sendTo == $OSCOM_Customer->getDefaultAddressID()) {
        $search_in_odoo = 'WebStore - ' . $OSCOM_Customer->getId();
      } else {
        $search_in_odoo = 'WebStore New address - ' . $this->sendTo;
      }

      $ids = $OSCOM_ODOO->odooSearch('ref', '=', $search_in_odoo, 'res.partner');

      $field_list = array('id');

      $id_odoo_customer_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $id_odoo_customer = $id_odoo_customer_array[0][id];

      return $id_odoo_customer;
    }

    public function save() {

      $OSCOM_Customer = Registry::get('Customer');
      $OSCOM_ODOO = Registry::get('Odoo');

// label to include in Odoo
      $OSCOM_ODOO->getLabelName('Customer');
      $labelId = $OSCOM_ODOO->getLabelId();
      $type_string = $OSCOM_ODOO->getLabelType();

//country
      $OSCOM_ODOO->getCustomerId($this->sendTo);
      $OSCOM_ODOO->getCountryIdCustomer($this->sendTo);
      $CountryIdOdoo = $OSCOM_ODOO->getCountryIdOdoo();

      if ($this->odooCreateShippingAddress == 1) {
        if (!is_null($this->getIdOdooPartner())) {

          $values = array (
                            'name' => new \xmlrpcval( $this->lastname . ' ' . $this->firstname, "string"),
                            'street'=>new \xmlrpcval($this->entryStreetAddress,"string"),
                            'street2'=>new \xmlrpcval($this->entrySuburb,"string"),
                            'city'=>new \xmlrpcval($this->entryCity,"string"),
                            'zip'=>new \xmlrpcval($this->entryPostcode,"string"),
                            'country_id'=>new \xmlrpcval($CountryIdOdoo,"int"),
                            'phone'=>new \xmlrpcval($this->entryTelephone,"string"),
                            'category_id' => new \xmlrpcval($labelId, $type_string),
                            'clicshopping_customers_additional_address_id' => new \xmlrpcval($this->sendTo,"int"),
                          );

          $OSCOM_ODOO->updateOdoo($this->getIdOdooPartner(), $values, 'res.partner');

        } else {

          $values = array (
                            "name" => new \xmlrpcval( $this->lastname . ' ' . $this->firstname, "string"),
                            'street'=>new \xmlrpcval($this->entryStreetAddress,"string"),
                            'street2'=>new \xmlrpcval($this->entrySuburb,"string"),
                            'city'=>new \xmlrpcval($this->entryCity,"string"),
                            'zip'=>new \xmlrpcval($this->entryPostcode,"string"),
                            'country_id'=>new \xmlrpcval($CountryIdOdoo,"int"),
                            "comment" => new \xmlrpcval('Web store New address book creation - relation with customer_id : ' . $OSCOM_Customer->getID() , "string"),
                            "ref" => new \xmlrpcval('WebStore New address - ' . $this->sendTo, "string"),
                            "phone"=>new \xmlrpcval($this->entryTelephone,"string"),
                            "category_id" => new \xmlrpcval($labelId, $type_string),
                            "clicshopping_customers_additional_address_id" => new \xmlrpcval($this->sendTo,"int"),
                          );

          $OSCOM_ODOO->createOdoo($values, 'res.partner');
        }
      }
    }
  } //end class
