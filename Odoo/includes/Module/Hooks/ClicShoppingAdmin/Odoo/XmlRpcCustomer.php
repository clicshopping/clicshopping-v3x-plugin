<?php
/*
   * AdditionalCheckoutButtons.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\ClicShoppingAdmin\Odoo;

  use ClicShopping\OM\Registry;
  use ClicShopping\OM\HTML;
  use ClicShopping\OM\OSCOM;

  class XmlRpcCustomer {

    public function __construct() {

      if (OSCOM::getSite() != 'ClicShoppingAdmin') {
        OSCOM::redirect('index.php', 'SSL');
      }


      $this->customer_id =  $_GET['cID'];
      $this->customers_newsletter = HTML::sanitize($_POST['customers_newsletter']);
      $this->customers_telephone = HTML::sanitize($_POST['customers_telephone']);
      $this->customers_cellular_phone = HTML::sanitize($_POST['customers_cellular_phone']);
      $this->customers_fax = HTML::sanitize($_POST['customers_fax']);
      $this->entry_street_address = HTML::sanitize($_POST['entry_street_address']);
      $this->entry_suburb = HTML::sanitize($_POST['entry_suburb']);
      $this->entry_postcode = HTML::sanitize($_POST['entry_postcode']);
      $this->entry_city = HTML::sanitize($_POST['entry_city']);
      $this->customers_lastname = HTML::sanitize($_POST['customers_lastname']);
      $this->customers_firstname = HTML::sanitize($_POST['customers_firstname']);
      $this->customers_email_address = HTML::sanitize($_POST['customers_email_address']);
      $this->customers_group_id = HTML::sanitize($_POST['customers_group_id']);
    }


    private function getIdOdooCustomer() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_customers_id', '=', $this->customer_id, 'res.partner');

      $field_list = array('id');

      $id_odoo_customer_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $id_odoo_customer = $id_odoo_customer_array[0][id];

      return $id_odoo_customer;
    }

    private function getIdOdooCustomerGroup() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_customers_group_id', '=', $this->customers_group_id, 'clicshopping.customers.group', 'int');

      $field_list = array('id');

      $id_odoo_customer_group__array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'clicshopping.customers.group');
      $id_odoo_customer_group = $id_odoo_customer_group__array[0][id];

      return $id_odoo_customer_group;
    }


    public function save() {
      $OSCOM_ODOO = Registry::get('Odoo');

// update newsletter
      if ($this->customers_newsletter == 1) $this->customers_newsletter = 0;

//country
      $OSCOM_ODOO->getCustomerId($this->customer_id);
      $CountryIdOdoo = $OSCOM_ODOO->getCountryIdOdoo();

// label to include in Odoo
      $OSCOM_ODOO->getLabelName('Customer');
      $labelId = $OSCOM_ODOO->getLabelId();
      $type_string = $OSCOM_ODOO->getLabelType();

//save
      if  (empty($this->getIdOdooCustomer())) {

        $values = array("ref" => new \xmlrpcval('WebStore - ' . $this->customer_id , "string"),
                        "phone" => new \xmlrpcval($this->customers_telephone,"string"),
                        "mobile" => new \xmlrpcval($this->customers_cellular_phone,"string"),
                        "fax" => new \xmlrpcval($this->customers_fax,"string"),
                        "street" => new \xmlrpcval($this->entry_street_address,"string"),
                        "street2"=> new \xmlrpcval($this->entry_suburb,"string"),
                        "zip" => new \xmlrpcval($this->entry_postcode,"string"),
                        "city" => new \xmlrpcval($this->entry_city,"string"),
                        "comment" => new \xmlrpcval('Website Registration - Admin Creation', "string"),
                        "name"    => new \xmlrpcval( $this->customers_lastname . ' ' . $this->customers_firstname, "string"),
                        "email"  => new \xmlrpcval($this->customers_email_address, "string"),
                        "country_id" => new \xmlrpcval($CountryIdOdoo, "int"),
                        "tz"      => new \xmlrpcval("Europe/Paris", "string"),
                        "clicshopping_customers_id" => new \xmlrpcval($this->customer_id , "int"),
                        "ClicShopping_customer_save_to_catalog" => new xmlrpcval(1, "int"),
                        "clicshopping_partner_customer_group_id"  => new \xmlrpcval($this->getIdOdooCustomerGroup(), "int"),
                        "category_id" => new \xmlrpcval($labelId, $type_string),
                      );

        $OSCOM_ODOO->createOdoo($values, "res.partner");

      } else {

// **********************************
// update Customer if exist
// **********************************

        $values = array(  "name"    => new \xmlrpcval( $this->customers_lastname . ' ' . $this->customers_firstname, "string"),
                          "email"  => new \xmlrpcval($this->customers_email_address, "string"),
                          'phone' => new \xmlrpcval($this->customers_telephone,"string"),
                          'mobile' => new \xmlrpcval($this->customers_cellular_phone,"string"),
                          'fax' => new \xmlrpcval($this->customers_fax,"string"),
                          'street' => new \xmlrpcval($this->entry_street_address,"string"),
                          'street2'=>new \xmlrpcval($this->entry_suburb,"string"),
                          'fax' => new \xmlrpcval($this->customers_fax,"string"),
                          'zip' => new \xmlrpcval($this->entry_postcode,"string"),
                          'city' => new \xmlrpcval($this->entry_city,"string"),
                          'country_id' => new \xmlrpcval($CountryIdOdoo,"int"),
                          "ref" => new \xmlrpcval('WebStore - ' . $this->customer_id , "string"),
                          "clicshopping_customers_id" => new \xmlrpcval($this->customer_id , "int"),
                          "ClicShopping_customer_save_to_catalog" => new xmlrpcval(1, "int"),
                          "clicshopping_partner_customer_group_id"  => new \xmlrpcval($this->getIdOdooCustomerGroup(), "int"),
                          "category_id" => new \xmlrpcval($labelId, $type_string),
                        );

        $OSCOM_ODOO->updateOdoo($this->getIdOdooCustomer(), $values, 'res.partner');
      }

    } // end save
  } //end class
