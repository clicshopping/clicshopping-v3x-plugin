<?php
/*
 * XmlRpcProducts.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\ClicShoppingAdmin\Odoo;

  use ClicShopping\OM\Registry;
  use ClicShopping\OM\HTML;
  use ClicShopping\Sites\ClicShoppingAdmin\ProductsAdmin;
  use ClicShopping\Sites\Common\HTMLOverrideCommon;
  use ClicShopping\OM\OSCOM;


  class XmlRpcProducts {
    public function __construct() {

      $OSCOM_Language = Registry::get('Language');
      $OSCOM_Db = Registry::get('Db');

      if (OSCOM::getSite() != 'ClicShoppingAdmin') {
        OSCOM::redirect('index.php', 'SSL');
      }

      $Qproduct = $OSCOM_Db->prepare('select p.*,
                                              pd.*
                                       from :table_products p,
                                            :table_products_description pd
                                       where p.products_id = :products_id
                                       and pd.products_id = p.products_id
                                       and pd.language_id = :language_id
                                      ');
      $Qproduct->bindInt(':products_id', $this->Id() );
      $Qproduct->bindInt(':language_id',  (int)$OSCOM_Language->getID() );
      $Qproduct->execute();

      $product = $Qproduct->fetch();

      $this->data = $product;
      $this->ProductId = $Qproduct->valueInt('products_id');
      $this->productsDescription  = HTMLOverrideCommon::stripHtmlTags($Qproduct->value('products_description'));
      $this->productsModel = $Qproduct->value('products_model');
      $this->productName = $Qproduct->value('products_name');
      $this->productPrice = $Qproduct->value('products_price');
      $this->productCost = $Qproduct->value('products_cost');
      $this->productEan = $Qproduct->value('products_ean');
      $this->productWharehouseLevelLocation = $Qproduct->value('products_wharehouse_level_location');
      $this->productWarehouse = $Qproduct->value('products_wharehouse');
      $this->productWarehouseRow = $Qproduct->value('products_wharehouse_row');
      $this->productWeight = $Qproduct->value('products_weight');
      $this->productHeadTitleTag = $Qproduct->value('products_head_title_tag');
      $this->productHeadDesTag = $Qproduct->value('products_head_desc_tag');
      $this->productHeadKeywordsTag = $Qproduct->value('products_head_keywords_tag');
      $this->productType = $Qproduct->value('products_type');
      $this->productSku = $Qproduct->value('products_sku');
      $this->productWeightPound = $Qproduct->value('products_weight_pounds');
      $this->productDimensionWidth = $Qproduct->value('products_dimension_width');
      $this->productDimensionHeight = $Qproduct->value('products_dimension_height');
      $this->productsDimensionDepth = $Qproduct->value('products_dimension_depth');
      $this->productsDimensionType = $Qproduct->value('products_dimension_type');
      $this->productsWharehouseTimeReplenishment = $Qproduct->value('products_wharehouse_time_replenishment');
      $this->productsDescriptionHtml = $Qproduct->value('products_description');
      $this->productsPriceComparison = $Qproduct->value('products_price_comparison');
      $this->productsOnlyOnline = $Qproduct->value('products_only_online');
      $this->productsOnlyShop = $Qproduct->value('products_only_shop');
      $this->productsStatus = $Qproduct->value('products_status');
      $this->productsQuantityAlert = $Qproduct->value('products_quantity_alert');
      $this->productsMinQtyOrder = $Qproduct->value('products_min_qty_order');
      $this->productsPriceKilo = $Qproduct->value('products_price_kilo');
      $this->productsView = $Qproduct->value('products_view');
      $this->productsHandling = $Qproduct->value('products_handling');
      $this->ordersView = $Qproduct->value('orders_view');
      $this->adminUserName = $Qproduct->value('admin_user_name');
      $this->productsDateAvailable = $Qproduct->value('products_date_available');
      $this->productsPercentage = $Qproduct->value('products_percentage');
      $this->productSupplierId = $Qproduct->valueInt('suppliers_id');
      $this->productTaxClassId = $Qproduct->valueInt('products_tax_class_id');
      $this->productQuantity = $Qproduct->value('products_quantity');
      $this->productManufacturersId = $Qproduct->value('manufacturers_id');
      $this->productPackaging = ProductsAdmin::GetproductPackaging();

      $this->date = date("Y-m-d H:i:s");

      // products_image export
      if (file_exists(DIR_FS_CATALOG_IMAGES . $Qproduct->value('products_image')) ) {
        $products_image = DIR_FS_CATALOG_IMAGES . $Qproduct->value('products_image');
        $data_small_image = file_get_contents($products_image);
        $this->productsImageOdoo = base64_encode($data_small_image);
      }

      if (file_exists(DIR_FS_CATALOG_IMAGES . $Qproduct->value('products_image_zoom')) ) {
        $products_image_zoom = DIR_FS_CATALOG_IMAGES . $Qproduct->value('products_image_zoom');
        $data_zoom_image = file_get_contents($products_image_zoom);
        $this->productsImageZoomOdoo = base64_encode($data_zoom_image);
      }
    }

/**
 * Select the ID Product of ClicShpping
 * @param string
 * @return  $products_id, id of product
 * @access private
 */
    private function Id() {

      if (is_numeric($_POST['products_id']) || is_numeric($_GET['pID']) ) {
        if (isset($_POST['products_id'])) {
          $_SESSION['ProductAdminId'] = HTML::sanitize($_POST['products_id']);
          $products_id = $_SESSION['ProductAdminId'] ;
        } else {
          if (isset($_GET['pID'])) {
            $_SESSION['ProductAdminId'] = HTML::sanitize($_GET['pID']);
            $products_id = $_SESSION['ProductAdminId'] ;
          }
        }

        return $products_id;
      }
    }

/**
 * Select the ID of company in Odoo
 * @param string
 * @return  $company_id, id of company odoo
 * @access public
 */
    private function getCompanyId() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $company_id = $OSCOM_ODOO->getSearchCompanyIdOdoo();

      return $company_id;
    }

/**
 * Select the ID Product in Odoo
 * @param string
 * @return  $products_id_odoo, id of product odoo
 * @access private
*/
    private function setProductsIdOdoo() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $products_id_odoo = $OSCOM_ODOO->getSearchProductIdOdoo();

      return $products_id_odoo;
    }
/**
 * Select the IDodoo categories
 * @param string
 * @return  $categories_id_odoo, id of categories
 * @access private
 */
    private function setCategoriesId() {
      $OSCOM_ODOO = Registry::get('Odoo');
      $OSCOM_Db = Registry::get('Db');

      $Qcategories_products  = $OSCOM_Db->prepare('select products_id,
                                                          categories_id
                                                    from :table_products_to_categories
                                                    where products_id = :products_id
                                                   ');
      $Qcategories_products->bindInt(':products_id', $this->ProductId );
      $Qcategories_products->execute();

      $categories_products_id = $Qcategories_products->valueInt('categories_id');

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_categories_id', '=', $categories_products_id , 'product.category', 'int');

// **********************************
// read id clicshopping_categories_id odoo
// **********************************

      $field_list = array('id');

      $Qcategories_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.category');
      $categories_id_odoo =   $Qcategories_id_odoo[0][id];

      if (empty($categories_id_odoo)) {
        $categories_id_odoo = 1;
      }

      return $categories_id_odoo;
    }

/**
 * Select the ID of the Odoo brand
 * @param string
 * @return  $manufacturer_id_odoo, id of the brand
 * @access private
 */
    private function getManufacturerId() {
      $OSCOM_ODOO = Registry::get('Odoo');

      if (!empty($this->productManufacturersId)) {

        $ids = $OSCOM_ODOO->odooSearch('clicshopping_manufacturers_id', '=', $this->productManufacturersId, 'clicshopping.manufacturer', 'int');

        $field_list = array('clicshopping_manufacturers_id');
        $Qmanufacturers_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'clicshopping.manufacturer');
        $manufacturer_id_odoo = $Qmanufacturers_id_odoo[0][id];
      }

      return $manufacturer_id_odoo;
    }

/**
 * create or update Odoo supplier
 * @param string
 * @return
 * @access private
 */
    private function getSupplier() {
      $OSCOM_ODOO = Registry::get('Odoo');

// **************************
// Search odoo suppliers id in res.partner
// ***************************

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_suppliers_id', '=', $this->productSupplierId, 'res.partner');

// **********************************
// read id suppliers_id odoo
// **********************************

      $field_list = array('id',
                          'name',
                         );

      $Qsuppliers_id_name_res_partner_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $suppliers_id_name_res_partner_odoo = $Qsuppliers_id_name_res_partner_odoo[0][id];


      if  (!empty($suppliers_id_name_res_partner_odoo)) {

// **********************************
// Search odoo suppliers Info id
// **********************************

        $ids =  $OSCOM_ODOO->odooSearchByTwoCriteria('product_code', '=', $this->productsModel, 'product.supplierinfo', 'string',
                                                     'product_tmpl_id', '=', $this->setProductsIdOdoo(), 'int');

// **********************************
// read id suppliers Info  Id odoo
// **********************************

        $field_list = array('id',
                            'name',
                           );

        $Qsuppliers_id_name_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.supplierinfo');
        $suppliers_id_name_odoo = $Qsuppliers_id_name_odoo[0][id];

        if  (empty($suppliers_id_name_odoo) ) {

          $values = array ( "name" => new \xmlrpcval($suppliers_id_name_res_partner_odoo, "int"),
                            "product_name" => new \xmlrpcval($this->productName, "string"),
                            "product_code" => new \xmlrpcval($this->productsModel, "string"),
                            "product_tmpl_id" => new \xmlrpcval($this->setProductsIdOdoo(), "int"),
                          );

          $OSCOM_ODOO->createOdoo($values, "product.supplierinfo");

        } else {

          $id_list = array();
          $id_list[] = new \xmlrpcval($suppliers_id_name_odoo, 'int');

          $values = array ( "name" => new \xmlrpcval($suppliers_id_name_res_partner_odoo, "int"),
                            "product_name" => new \xmlrpcval($this->productName, "string"),
                            "product_code" => new \xmlrpcval($this->productsModel,"string"),
                            "product_tmpl_id" => new \xmlrpcval($this->setProductsIdOdoo(), "int"),
                          );

          $OSCOM_ODOO->updateOdoo($suppliers_id_name_odoo, $values, 'product.supplierinfo');
        }

      } else {

        $values = array("name" => new \xmlrpcval($suppliers_id_name_res_partner_odoo, "int"),
                        "product_name" => new \xmlrpcval($this->productName, "string"),
                        "product_code" => new \xmlrpcval($this->productsModel, "string"),
                        "product_tmpl_id" => new \xmlrpcval($this->setProductsIdOdoo(), "int"),
                      );

        $OSCOM_ODOO->createOdoo($values, "product.supplierinfo");
      }

    }


    private function getStockWharehouseCode() {
      $OSCOM_ODOO = Registry::get('Odoo');

// stock warehouse search id and code concerning ClicShopping Wharehouse
      $ids = $OSCOM_ODOO->odooSearch('name', '=', $OSCOM_ODOO->getWarehouseName(), 'stock.warehouse');

      $field_list = array('code');

      $Qstock_wharehouse = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.warehouse');

      $stock_wharehouse_code = $Qstock_wharehouse[0][code];

      return $stock_wharehouse_code;
    }





    private function getWharehouseLocationId() {
      $OSCOM_ODOO = Registry::get('Odoo');
// search location name and id in stock location
      $ids = $OSCOM_ODOO->odooSearch('name', '=', $this->getStockWharehouseCode(), 'stock.location');

      $field_list = array('id');

      $Qstock_location = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.location');
      $stock_location_id = $Qstock_location[0][id];

      return $stock_location_id;
    }

    private function getWharehouseLocationName() {
      $OSCOM_ODOO = Registry::get('Odoo');
// search location name and id in stock location
      $ids = $OSCOM_ODOO->odooSearch('name', '=', $this->getStockWharehouseCode(), 'stock.location');

      $field_list = array('name');

      $Qstock_location = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.location');

      $stock_location_name = $Qstock_location[0][name];

      return $stock_location_name;
    }




    private function getStockAvailabe() {

      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('name', '=', $this->getStockWharehouseCode(), 'stock.location');

      $field_list = array('qty_available');

      $Qproducts_stock_available = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
      $products_stock_available = $Qproducts_stock_available[0][qty_available];

      return $products_stock_available;
    }


// stock minimum in odoo
    private function getStockMininum() {

      $OSCOM_ODOO = Registry::get('Odoo');

      if ($this->productQuantity != $this->getStockAvailabe() ) {
        // hack, don't find the good value in location.stock. Add +1 on id because it's create just after
        if (ODOO_WEB_SERVICE_COMPANY_WEB_SERVICE != 'Your Company') {
          $stock_location_id = $this->getWharehouseLocationId() + 1;
        } else {
          $stock_location_id = $this->getWharehouseLocationId();
        }

      }

      $ids = $OSCOM_ODOO->odooSearch('product_id', '=', $this->setProductsIdOdoo(), 'stock.warehouse.orderpoint', 'int' );

      $field_list = array ('id');

      $Qodoo_min_stock = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.warehouse.orderpoint');
      $odoo_min_stock_id = $Qodoo_min_stock[0][id];

      if ($this->productsQuantityAlert == 0) {
        $stock_alert = STOCK_REORDER_LEVEL;
      } else {
        $stock_alert = $this->productsQuantityAlert;
      }

      if (!empty($odoo_min_stock_id)) {

        $id_list = array();
        $id_list[]= new \xmlrpcval($odoo_min_stock_id, 'int');

        $values = array("product_min_qty" => new \xmlrpcval($stock_alert, "double") );

        $OSCOM_ODOO->updateOdoo($odoo_min_stock_id, $values, 'stock.warehouse.orderpoint');

      } else {
        $values = array(
                          "name" => new \xmlrpcval($this->productsModel . ' / ' . $this->ProductId, "string"), // qty of my product
                          "product_id" => new \xmlrpcval($this->setProductsIdOdoo(), "int"), //
                          "warehouse_id" => new \xmlrpcval($OSCOM_ODOO->getStockWharehouseId(), "int"),
                          "location_id" => new \xmlrpcval($stock_location_id, "int"), // Virtual Locations/Inventory loss
                          "product_min_qty" => new \xmlrpcval($stock_alert, "double"),
                          "product_max_qty" => new \xmlrpcval(0, "double"),
                          "qty_multiple" => new \xmlrpcval(1, "double"),
                          "company_id" => new \xmlrpcval($this->getCompanyId(), "int"),
                          "active" => new \xmlrpcval(1, "double"),
                        );

        $OSCOM_ODOO->createOdoo($values, "stock.warehouse.orderpoint");
      }
    }


//*************************************
// update stock
//*************************************

    private function getStock() {

      $OSCOM_ODOO = Registry::get('Odoo');

      $products_name_odoo = '[' . $this->productsModel . '] ' . $this->productName;

        if (!empty($this->getWharehouseLocationId())) {

          if ($this->productQuantity != $this->getStockAvailabe() ) {

// search qty in stock.quant
            $ids = $OSCOM_ODOO->odooSearchAllByTwoCriteria('product_id', '=', $products_name_odoo, 'stock.quant', 'string',
                                                           'location_id', 'like', $this->getWharehouseLocationName() . '%', 'string');

            $field_list = array('qty');

            $QSearchStock = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.quant');

            $stock_odoo_qty_total = '0';

// Stock calcul inside wharehouse
            foreach ($QSearchStock as $key) {
              $products_stock_qty = $key[qty];
              $stock_odoo_qty_total = $stock_odoo_qty_total + $products_stock_qty;
            }

// difference between ClicShopping and Odoo
            $new_stock = $this->productQuantity - $stock_odoo_qty_total;

// hack, don't find the good value in location.stock. Add +1 on id because it's create just after
            if (ODOO_WEB_SERVICE_COMPANY_WEB_SERVICE != 'Your Company') {
              $stock_location_id = $this->getWharehouseLocationId() + 1;
            } else {
              $stock_location_id = $this->getWharehouseLocationId();
            }


// move stock

            if ($new_stock > 0) {
              $values = array("product_id" => new \xmlrpcval($this->setProductsIdOdoo(), "int"),
                                "product_uom_qty" => new \xmlrpcval($new_stock, "double"), // qty of my product
                                "name" => new \xmlrpcval($products_name_odoo, "string"),
                                "invoice_state" => new \xmlrpcval('none', "string"),
                                "date" => new \xmlrpcval($this->date, "string"),
                                "date_expected" => new \xmlrpcval($this->date, "string"),
                                "company_id" => new \xmlrpcval($this->getCompanyId(), "int"),
                                "procure_method" => new \xmlrpcval('make_to_stock', "string"),
                                "location_dest_id" => new \xmlrpcval($stock_location_id, "string"),
                                "location_id" => new \xmlrpcval(5, "int"), // Virtual Locations/Inventory loss
                                "product_uom" => new \xmlrpcval(1, "int"),
                                "origin" => new \xmlrpcval("ClicShopping Webstore", "string"),
                              );
            } else {

// Adapted for odoo on the calcul
              $new_stock = $new_stock * (-1);

              $values = array(
                                "product_id" => new \xmlrpcval($this->setProductsIdOdoo(), "int"),
                                "product_uom_qty" => new \xmlrpcval($new_stock, "double"), // qty of my product
                                "name" => new \xmlrpcval($products_name_odoo, "string"),
                                "invoice_state" => new \xmlrpcval('none', "string"),
                                "date" => new \xmlrpcval($this->date, "string"),
                                "date_expected" => new \xmlrpcval($this->date, "string"),
                                "company_id" => new \xmlrpcval($this->getCompanyId(), "int"),
                                "procure_method" => new \xmlrpcval('make_to_stock', "string"),
                                "location_dest_id" => new \xmlrpcval(5, "string"),
                                "location_id" => new \xmlrpcval($stock_location_id, "int"), // Virtual Locations/Inventory loss
                                "product_uom" => new \xmlrpcval(1, "int"),
                                "origin" => new \xmlrpcval("ClicShopping Webstore", "string"),
                              );
            }

            $OSCOM_ODOO->createOdoo($values, "stock.move");


// search id for stock.move concerning the product
            $ids = $OSCOM_ODOO->odooSearchAllByTwoCriteria('product_id', '=', $this->setProductsIdOdoo(), 'stock.move', 'int',
                                                          'date', '=', $this->date, 'string');

            $field_list = array('id');

            $Qodoo_move_read = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.move');
            $odoo_picking_id = $Qodoo_move_read[0][id];

            $OSCOM_ODOO->buttonClickOdoo('stock.move', 'action_done', $odoo_picking_id);
          }
        } // end !empty($stock_location_id)
    }




/**
 * create or update Odoo article
 * @param string
 * @return
 * @access public
 */
    public function save() {

      $OSCOM_ODOO = Registry::get('Odoo');
      $OSCOM_Db = Registry::get('Db');

// **************************************************************************
// Search odoo products tax
// **************************************************************************

      $QProductsCodeTax = $OSCOM_Db->prepare('select distinct tax_class_id,
                                                                code_tax_odoo
                                               from :table_tax_rates
                                               where tax_class_id = :tax_class_id
                                              ');

      $QProductsCodeTax->bindValue(':tax_class_id', $this->productTaxClassId);

      $QProductsCodeTax->execute();

      $products_code_tax = $QProductsCodeTax->value('code_tax_odoo');

// research id tax by description
      if ($products_code_tax != null) {
        $ids = $OSCOM_ODOO->odooSearch('description', '=', $products_code_tax, 'account.tax', 'string');
        $odoo_products_tax_id = $ids;
        $odoo_products_tax_id = $odoo_products_tax_id[0];

        if (!empty($odoo_products_tax_id)) {

          $type_tax_string = 'array';

          $tax = array(new \xmlrpcval(
                                      array(
                                              new \xmlrpcval(6, "int"),// 6 : id link
                                              new \xmlrpcval(0, "int"),
                                              new \xmlrpcval(array(new \xmlrpcval($odoo_products_tax_id, "int")), "array")
                                            ), "array"
                                    )
                      );
        } else  {
          $tax = 0;
          $type_tax_string = 'int';
        }

      } else {
        $tax = 0;
        $type_tax_string = 'int';
      }


        if  ($this->setProductsIdOdoo() == null || empty($this->setProductsIdOdoo())) {
// Create products if doesn't exist in odoo
          $values = array (  "default_code" => new \xmlrpcval($this->productsModel, "string"),
                              "name" => new \xmlrpcval($this->productName,"string"),
                              "categ_id" => new \xmlrpcval($this->setCategoriesId(), "int"),
                              "description"  => new \xmlrpcval($this->productsDescription,"string"),
                              "description_purchase"  => new \xmlrpcval($this->productsDescription,"string"),
                              "description_sale" => new \xmlrpcval($this->productName,"string"),
                              "list_price" => new \xmlrpcval($this->productPrice,"double"),
                              "standard_price" => new \xmlrpcval($this->productCost,"double"),
                              "image" => new \xmlrpcval($this->productsImageZoomOdoo,"string"),
                              "image_medium" => new \xmlrpcval($this->productsImageZoomOdoo,"string"),
                              "image_small" => new \xmlrpcval($this->productsImageOdoo,"string"),
                              "ean13" => new \xmlrpcval($this->productEan,"string"),
                              "loc_case" => new \xmlrpcval($this->productWharehouseLevelLocation,"string"),
                              "loc_rack" => new \xmlrpcval($this->productWarehouse,"string"),
                              "loc_row" => new \xmlrpcval($this->productWarehouseRow,"string"),
                              "weight" => new \xmlrpcval($this->productWeight,"double"),
                              "website_meta_title" => new \xmlrpcval($this->productHeadTitleTag,"string"),
                              "website_meta_description" => new \xmlrpcval($this->productHeadDesTag,"string"),
                              "website_meta_keywords" => new \xmlrpcval($this->productHeadKeywordsTag,"string"),
                              "taxes_id" =>  new \xmlrpcval($tax, $type_tax_string ),
                              "type" =>  new \xmlrpcval($this->productType, "string"),
                              "clicshopping_products_sku" => new \xmlrpcval($this->productSku,"string"),
                              "clicshopping_products_id" =>  new \xmlrpcval($this->ProductId, "int"),
                              "ClicShopping_products_save_to_catalog" => new xmlrpcval(1, "int"),
                              "clicshopping_products_weight_pounds" => new \xmlrpcval($this->productWeightPound,"double"),
                              "clicshopping_products_dimension_width" => new \xmlrpcval($this->productDimensionWidth,"double"),
                              "clicshopping_products_dimension_height" => new \xmlrpcval($this->productDimensionHeight,"double"),
                              "clicshopping_products_dimension_depth" => new \xmlrpcval($this->productsDimensionDepth,"double"),
                              "clicshopping_products_dimension_type" => new \xmlrpcval($this->productsDimensionType,"string"),
                              "clicshopping_products_wharehouse_time_replenishment" => new \xmlrpcval($this->productsWharehouseTimeReplenishment,"string"),
                              "clicshopping_products_description" => new \xmlrpcval($this->productsDescriptionHtml,"string"),
                              "clicshopping_products_price_comparison" => new \xmlrpcval($this->productsPriceComparison,"double"),
                              "clicshopping_products_only_online" => new \xmlrpcval($this->productsOnlyOnline,"double"),
                              "clicshopping_products_only_shop" => new \xmlrpcval($this->productsOnlyShop,"double"),
                              "clicshopping_products_stock_status" => new \xmlrpcval($this->productsStatus,"double"),
                              "clicshopping_products_quantity_alert" => new \xmlrpcval($this->productsQuantityAlert,"double"),
                              "clicshopping_products_min_qty_order" => new \xmlrpcval($this->productsMinQtyOrder,"double"),
                              "clicshopping_products_price_kilo" => new \xmlrpcval($this->productsPriceKilo,"double"),
                              "clicshopping_products_view" => new \xmlrpcval($this->productsView,"double"),
                              "clicshopping_products_handling" => new \xmlrpcval($this->productsHandling,"double"),
                              "clicshopping_products_orders_view" => new \xmlrpcval($this->ordersView,"double"),
                              "clicshopping_admin_user_name" => new \xmlrpcval($this->adminUserName,"string"),
                              "clicshopping_products_date_available" => new \xmlrpcval($this->productsDateAvailable,"string"),
                              "clicshopping_products_packaging" => new \xmlrpcval($this->productPackaging,"string"),
                              "clicshopping_products_percentage" => new \xmlrpcval($this->productsPercentage,"double"),
                              "clicshopping_product_manufacturer_id" => new \xmlrpcval($this->getManufacturerId(),"int"),
                           );

          $OSCOM_ODOO->createOdoo($values, "product.template");

        }  else {

// **********************************
// update products if exist
// **********************************

          $id_list = array();
          $id_list[]= new \xmlrpcval($this->setProductsIdOdoo(), 'int');

          $values = array ( "default_code" => new \xmlrpcval($this->productsModel, "string"),
                            "name" => new \xmlrpcval($this->productName,"string"),
                            "categ_id" => new \xmlrpcval($this->setCategoriesId(), "int"),
                            "description"  => new \xmlrpcval($this->productsDescription,"string"),
                            "description_purchase"  => new \xmlrpcval($this->productsDescription,"string"),
                            "description_sale" => new \xmlrpcval($this->productName,"string"),
                            "list_price" => new \xmlrpcval($this->productPrice,"double"),
                            "standard_price" => new \xmlrpcval($this->productCost,"double"),
                            "image" => new \xmlrpcval($this->productsImageZoomOdoo,"string"),
                            "image_medium" => new \xmlrpcval($this->productsImageZoomOdoo,"string"),
                            "image_small" => new \xmlrpcval($this->productsImageOdoo,"string"),
                            "ean13" => new \xmlrpcval($this->productEan,"string"),
                            "loc_case" => new \xmlrpcval($this->productWharehouseLevelLocation,"string"),
                            "loc_rack" => new \xmlrpcval($this->productWarehouse,"string"),
                            "loc_row" => new \xmlrpcval($this->productWarehouseRow,"string"),
                            "weight" => new \xmlrpcval($this->productWeight,"double"),
                            "website_meta_title" => new \xmlrpcval($this->productHeadTitleTag,"string"),
                            "website_meta_description" => new \xmlrpcval($this->productHeadDesTag,"string"),
                            "website_meta_keywords" => new \xmlrpcval($this->productHeadKeywordsTag,"string"),
                            "taxes_id" =>  new \xmlrpcval($tax, $type_tax_string ),
                            "type" =>  new \xmlrpcval($this->productType, "string"),
                            "clicshopping_products_id" =>  new \xmlrpcval($this->ProductId, "int"),
                            "ClicShopping_products_save_to_catalog" => new xmlrpcval(1, "int"),
                            "clicshopping_products_sku" => new \xmlrpcval($this->productSku,"string"),
                            "clicshopping_products_weight_pounds" => new \xmlrpcval($this->productWeightPound,"double"),
                            "clicshopping_products_dimension_width" => new \xmlrpcval($this->productDimensionWidth,"double"),
                            "clicshopping_products_dimension_height" => new \xmlrpcval($this->productDimensionHeight,"double"),
                            "clicshopping_products_dimension_depth" => new \xmlrpcval($this->productsDimensionDepth,"double"),
                            "clicshopping_products_dimension_type" => new \xmlrpcval($this->productsDimensionType,"string"),
                            "clicshopping_products_wharehouse_time_replenishment" => new \xmlrpcval($this->productsWharehouseTimeReplenishment,"string"),
                            "clicshopping_products_description" => new \xmlrpcval($this->productsDescriptionHtml,"string"),
                            "clicshopping_products_price_comparison" => new \xmlrpcval($this->productsPriceComparison,"double"),
                            "clicshopping_products_only_online" => new \xmlrpcval($this->productsOnlyOnline,"double"),
                            "clicshopping_products_only_shop" => new \xmlrpcval($this->productsOnlyShop,"double"),
                            "clicshopping_products_stock_status" => new \xmlrpcval($this->productsStatus,"double"),
                            "clicshopping_products_quantity_alert" => new \xmlrpcval($this->productsQuantityAlert,"double"),
                            "clicshopping_products_min_qty_order" => new \xmlrpcval($this->productsMinQtyOrder,"double"),
                            "clicshopping_products_price_kilo" => new \xmlrpcval($this->productsPriceKilo,"double"),
                            "clicshopping_products_view" => new \xmlrpcval($this->productsView,"double"),
                            "clicshopping_products_handling" => new \xmlrpcval($this->productsHandling,"double"),
                            "clicshopping_products_orders_view" => new \xmlrpcval($this->ordersView,"double"),
                            "clicshopping_admin_user_name" => new \xmlrpcval($this->adminUserName,"string"),
                            "clicshopping_products_date_available" => new \xmlrpcval($this->productsDateAvailable,"string"),
                            "clicshopping_products_packaging" => new \xmlrpcval($this->productPackaging,"string"),
                            "clicshopping_products_percentage" => new \xmlrpcval($this->productsPercentage,"double"),
                            "clicshopping_product_manufacturer_id" => new \xmlrpcval($this->getManufacturerId(),"int"),
                          );


          $OSCOM_ODOO->updateOdoo($this->setProductsIdOdoo(), $values, 'product.template');
        }

// Supplier
      if (!empty($this->productSupplierId)) {
        $this->getSupplier();
      }

//  Stock
      if (!empty($this->productQuantity)) {
        $this->getStock();
      }

// Stock minimum
      $this->getStockMininum();

      unset($_SESSION['ProductAdminId']);
    }
  }