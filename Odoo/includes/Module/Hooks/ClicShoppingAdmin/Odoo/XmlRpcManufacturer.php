<?php
/*
   * AdditionalCheckoutButtons.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\ClicShoppingAdmin\Odoo;

  use ClicShopping\OM\Registry;
  use ClicShopping\OM\HTML;
  use ClicShopping\OM\OSCOM;

  class XmlRpcManufacturer {

    public function __construct() {

      if (OSCOM::getSite() != 'ClicShoppingAdmin') {
        OSCOM::redirect('index.php', 'SSL');
      }

      $this->manufacturers_id = HTML::sanitize($_POST['manufacturers_id']);
      $this->manufacturers_name = HTML::sanitize($_POST['manufacturers_name']);
/* pb trim
      $this->manufacturer_description = HTML::sanitize($_POST['manufacturer_description']);
      $this->manufacturer_seo_title = HTML::sanitize($_POST['manufacturer_seo_title']);
      $this->manufacturer_seo_title = HTML::sanitize($_POST['manufacturer_seo_title']);
      $this->manufacturer_seo_description = HTML::sanitize($_POST['manufacturer_seo_description']);
      $this->manufacturer_seo_keyword = HTML::sanitize($_POST['manufacturer_seo_keyword']);
*/

      $this->manufacturer_description = $_POST['manufacturer_description'];
      $this->manufacturer_seo_title = $_POST['manufacturer_seo_title'];
      $this->manufacturer_seo_title = $_POST['manufacturer_seo_title'];
      $this->manufacturer_seo_description = $_POST['manufacturer_seo_description'];
      $this->manufacturer_seo_keyword = $_POST['manufacturer_seo_keyword'];


      $this->manufacturers_image = HTML::sanitize($_POST['manufacturers_image']);
    }

    private function getManufacturerImage() {

      if (!empty($this->manufacturers_image)) {
        if (file_exists(DIR_FS_CATALOG_IMAGES . $this->manufacturers_image ) ) {
          $manufacturers_image = DIR_FS_CATALOG_IMAGES . $this->manufacturers_image;
          $manufacturers_image = file_get_contents($this->manufacturers_image);
          $manufacturers_image = base64_encode($this->manufacturers_image);
        }
      }
      return $manufacturers_image;
    }

    private function getBrandIdOdoo() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_manufacturers_id', '=', $this->manufacturers_id, 'clicshopping.manufacturer', 'int');

      $field_list = array('clicshopping_manufacturers_id');

      $id_odoo_manufacturer_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'clicshopping.manufacturer');
      $id_odoo_manufacturer = $id_odoo_manufacturer_array[0][id];

      return $id_odoo_manufacturer;
    }

    public function save() {

      $OSCOM_ODOO = Registry::get('Odoo');

      if  (is_null($this->getBrandIdOdoo())) {

// **********************************
// Create manufacturer if doesn't exist in oddo
// **********************************
        $values = array(  "clicshopping_manufacturers_id" => new \xmlrpcval($this->manufacturers_id, "int"),
                          "ClicShopping_manufacturers_save_to_catalog" => new xmlrpcval(1, "int"),
                          "clicshopping_manufacturers_name" => new \xmlrpcval($this->manufacturers_name, "string"),
                          "clicshopping_manufacturer_description" => new \xmlrpcval($this->manufacturer_description, "string"),
                          "clicshopping_manufacturers_image" => new \xmlrpcval($this->getManufacturerImage(), "string"),
                          "clicshopping_manufacturer_seo_title" => new \xmlrpcval($this->manufacturer_seo_title, "string"),
                          "clicshopping_manufacturer_seo_description" => new \xmlrpcval($this->manufacturer_seo_description, "string"),
                          "clicshopping_manufacturer_seo_keyword" => new \xmlrpcval($this->manufacturer_seo_keyword, "string"),
                        );

        $OSCOM_ODOO->createOdoo($values, "clicshopping.manufacturer");

      } else {

// update manufacturer if exist
        $values = array(  "clicshopping_manufacturers_name" => new \xmlrpcval($this->manufacturers_name, "string"),
                          "ClicShopping_manufacturers_save_to_catalog" => new xmlrpcval(1, "int"),
                          "clicshopping_manufacturer_description" => new \xmlrpcval($this->manufacturer_description, "string"),
                          "clicshopping_manufacturers_image" => new \xmlrpcval($this->getManufacturerImage(), "string"),
                          "clicshopping_manufacturer_seo_title" => new \xmlrpcval($this->manufacturer_seo_title, "string"),
                          "clicshopping_manufacturer_seo_description" => new \xmlrpcval($this->manufacturer_seo_description, "string"),
                          "clicshopping_manufacturer_seo_keyword" => new \xmlrpcval($this->manufacturer_seo_keyword, "string"),
                        );

        $OSCOM_ODOO->updateOdoo($this->getBrandIdOdoo(), $values, 'clicshopping.manufacturer');
      }
    } // end save
  } //end class
