<?php
/*
 * xml_rpc_admin_customers.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/


  namespace ClicShopping\OM\Module\Hooks\ClicShoppingAdmin\Odoo;

  use ClicShopping\OM\Registry;
  use ClicShopping\OM\HTML;
  use ClicShopping\OM\OSCOM;

  class XmlRpcCustomersGroup {

    public function __construct() {

      if (OSCOM::getSite() != 'ClicShoppingAdmin') {
        OSCOM::redirect('index.php', 'SSL');
      }

      $this->customersGroupsName = HTML::sanitize($_POST['customers_group_name']);
      $this->customersGroupsDiscount = HTML::sanitize($_POST['customers_group_discount']);
      $this->colorBar = $_POST['color_bar'];
      $this->customersGroupQuantityDefault = HTML::sanitize($_POST['customers_group_quantity_default']);
      $this->groupOrderTaxe = HTML::sanitize($_POST['group_order_taxe']);
      $this->groupTax = HTML::sanitize($_POST['group_tax']);
    }

    private function GetCustomerGrouId() {
      if (isset($_GET['cID']) && is_numeric($_GET['cID'])) {
        $customer_group_id = HTML::sanitize($_GET['cID']);
      } else {
        $customer_group_id = HTML::sanitize($_POST['cID']);
      }
      return $customer_group_id;
    }

    private function getOdooCustomersGroupId() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_customers_group_id', '=', $this->GetCustomerGrouId(), 'clicshopping.customers.group', 'int');

      $field_list = array('id');

      $QodooCustomersGroupId = $OSCOM_ODOO->readOdoo($ids, $field_list, "clicshopping.customers.group");
      $odoo_customers_group_id = $QodooCustomersGroupId[0][id];

      return $odoo_customers_group_id;
    }

    public function save() {
      $OSCOM_ODOO = Registry::get('Odoo');

      if  (empty($this->getOdooCustomersGroupId())) {
// Create products if doesn't exist in odoo

        $values = array ("clicshopping_customers_group_name" => new \xmlrpcval($this->customersGroupsName, "string"),
                         "clicshopping_customers_group_id" => new \xmlrpcval($this->GetCustomerGrouId(), "int"),
                         "clicshopping_customers_group_discount" => new \xmlrpcval($this->customersGroupsDiscount, "double"),
                         "clicshopping_customers_group_color_bar" => new \xmlrpcval($this->colorBar, "string"),
                         "clicshopping_customers_group_quantity_default" => new \xmlrpcval($this->customersGroupQuantityDefault, "int"),
                         "clicshopping_customers_group_order_taxe" => new \xmlrpcval($this->groupOrderTaxe, "double"),
                         "clicshopping_customers_group_tax" => new \xmlrpcval($this->groupTax, "double"),
                        );

        $OSCOM_ODOO->createOdoo($values, "clicshopping.customers.group");

      }  else {
// update products if exist

        $id_list = array();
        $id_list[]= new \xmlrpcval($this->getOdooCustomersGroupId(), 'int');

        $values = array ( "clicshopping_customers_group_name" => new \xmlrpcval($this->customersGroupsName, "string"),
                          "clicshopping_customers_group_id" => new \xmlrpcval($this->GetCustomerGrouId(), "int"),
                          "clicshopping_customers_group_discount" => new \xmlrpcval($this->customersGroupsDiscount, "double"),
                          "clicshopping_customers_group_color_bar" => new \xmlrpcval($this->colorBar, "string"),
                          "clicshopping_customers_group_quantity_default" => new \xmlrpcval($this->customersGroupQuantityDefault, "int"),
                          "clicshopping_customers_group_group_order_taxe" => new \xmlrpcval($this->groupOrderTaxe, "double"),
                          "clicshopping_customers_group_tax" => new \xmlrpcval($this->groupTax, "double"),
                        );
        $OSCOM_ODOO->updateOdoo($this->getOdooCustomersGroupId(), $values, "clicshopping.customers.group");
      }
    }
  }
