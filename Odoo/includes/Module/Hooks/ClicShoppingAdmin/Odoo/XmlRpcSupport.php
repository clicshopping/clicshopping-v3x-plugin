<?php
/*
 * xml_rpc_admin_support.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\ClicShoppingAdmin\Odoo;

  use ClicShopping\OM\HTTP;
  use ClicShopping\OM\Registry;
  use ClicShopping\OM\OSCOM;

  class XmlRpcSupport  {

    private $user;
    private $password;
    private $database;
    private $server_url;
    private $response;
    private $id;

    function __construct() {

      $OSCOM_Mail = Registry::get('Mail');

      if (OSCOM::getSite() != 'ClicShoppingAdmin') {
        OSCOM::redirect('index.php', 'SSL');
      }

      $this->user = ODOO_USER_WEB_SERVICE_SUPPORT;
      $this->password = ODOO_PASSWORD_WEB_SERVICE_SUPPORT;;
      $this->database = ODOO_DATABASE_NAME_WEB_SERVICE_SUPPORT;;
      $this->server_url = ODOO_WEBSERVER_WEB_SERVICE_SUPPORT . ':' . ODOO_PORT_WEB_SERVICE_SUPPORT;;


      include_once("ext/api/xml_rpc/xmlrpc.inc.php");
      include_once("ext/api/xml_rpc/xmlrpcs.inc.php");
      $GLOBALS['xmlrpc_internalencoding']='UTF-8';

      $connexion = new \xmlrpc_client($this->server_url . '/xmlrpc/common');
      $connexion->setSSLVerifyPeer(0);

      $c_msg = new \xmlrpcmsg('login');
      $c_msg->addParam(new \xmlrpcval($this->database, "string"));
      $c_msg->addParam(new \xmlrpcval($this->user, "string"));
      $c_msg->addParam(new \xmlrpcval($this->password, "string"));
      $this->response = $connexion->send($c_msg);

      if ($this->response->errno == 0 ){
        $this->id = $this->response->value()->scalarval();
      }  else {
        if (ODOO_EMAIL_WEB_SERVICE == 'true') {
          $email_subject = 'Odoo webservice Error';
          $body_message = 'There is an error with odoo webservice on ' . HTTP::TypeUrlDomain() . ' at ' . date('Y-m-d H:i:s') . '  - message webservice odoo - Support Access - error : ';
          $OSCOM_Mail->oscMail(STORE_NAME, STORE_OWNER_EMAIL_ADDRESS, $email_subject, $body_message .'<br /><br /> ', STORE_NAME, STORE_OWNER_EMAIL_ADDRESS, '');
        }

        return -1;
      }


      $this->supportSubject = HTML::sanitize($_POST['support_subject']);
      $this->typeSupport = HTML::sanitize($_POST['type_support']);
      $this->messageSupport = HTML::sanitize($_POST['message_support']);
      $this->nameOdoCustomer = $_POST['name_odoo_customer'];
      $this->zipOdooCustomer = $_POST['zip_odoo_customer'];
    }

    public function traverse_structure_support($ids) {
      $return_ids = array();
      $iterator = new RecursiveArrayIterator($ids);

      while ( $iterator -> valid() ) {
        if ( $iterator -> hasChildren() ) {
          $return_ids = array_merge( $return_ids, $this->traverse_structure_support($iterator -> getChildren()) );
        } else {
          if ($iterator -> key() == 'int') {
            $return_ids = array_merge( $return_ids, array( $iterator -> current() ) );
          }
        }
        $iterator -> next();
      }
      return $return_ids;
    }

/*
@string : $attribute, fields of odoo databasee
@string : $operator, search type
@string : $keys, valueto search
@string : $relation, odoo database
]return : $ids , id and name of the odoo country
*/
    Public function odooSearchSupport($attribute, $operator, $keys, $relation, $string = 'string', $offset = 0, $limit = 1 ) {

      $client = new \xmlrpc_client($this->server_url . "/xmlrpc/object");
      $client->setSSLVerifyPeer(0);

      $domain_filter = array (
                                new \xmlrpcval(
                                                array(
                                                  new \xmlrpcval($attribute , "string"),
                                                  new \xmlrpcval($operator,"string"),
                                                  new \xmlrpcval($keys, $string),
                                                ),"array"
                                              ),
                              );

      $msg = new \xmlrpcmsg('execute');
      $msg->addParam(new \xmlrpcval($this->database, "string"));
      $msg->addParam(new \xmlrpcval($this->id, "int"));
      $msg->addParam(new \xmlrpcval($this->password, "string"));
      $msg->addParam(new \xmlrpcval($relation, "string"));
      $msg->addParam(new \xmlrpcval("search", "string"));
      $msg->addParam(new \xmlrpcval($domain_filter, "array"));
      $msg->addParam(new \xmlrpcval($offset, "int")); // OFFSET, START FROM
      $msg->addParam(new \xmlrpcval($limit, "int")); // MAX RECORD LIMITS

      $response = $client->send($msg);

      $val = $response->value();
      $ids = $val->scalarval();

      return $this->traverse_structure_support($ids);
    }


    Public function odooSearchByTwoCriteriaSupport($attribute, $operator, $keys, $relation, $string = 'string',  $attribute1, $operator1, $keys1, $string1 = 'string') {

      $client = new \xmlrpc_client($this->server_url . "/xmlrpc/object");
      $client->setSSLVerifyPeer(0);

      $domain_filter = array (
                              new \xmlrpcval(
                                            array(
                                              new \xmlrpcval($attribute , "string"),
                                              new \xmlrpcval($operator, "string"),
                                              new \xmlrpcval($keys, $string),
                                            ),"array"
                                           ),
                              new \xmlrpcval(
                                            array(
                                              new \xmlrpcval($attribute1 , "string"),
                                              new \xmlrpcval($operator1, "string"),
                                              new \xmlrpcval($keys1, $string1),
                                            ),"array"
                                          ),
                            );

      $msg = new \xmlrpcmsg('execute');
      $msg->addParam(new \xmlrpcval($this->database, "string"));
      $msg->addParam(new \xmlrpcval($this->id, "int"));
      $msg->addParam(new \xmlrpcval($this->password, "string"));
      $msg->addParam(new \xmlrpcval($relation, "string"));
      $msg->addParam(new \xmlrpcval("search", "string"));
      $msg->addParam(new \xmlrpcval($domain_filter, "array"));

      $response = $client->send($msg);

      $val = $response->value();
      $ids = $val->scalarval();

      return $this->traverse_structure_support($ids);
    }

    public function readOdooSupport($ids, $fields, $relation) {
      $client = new \xmlrpc_client($this->server_url . "/xmlrpc/object");
      $client->setSSLVerifyPeer(0);

      $client->return_type = 'phpvals';

      $id_val = array();
      $count = 0;

      foreach ($ids as $id) {
        $id_val[$count++] = new \xmlrpcval($id, "int");
      }

      $fields_val = array();

      $count = 0;

      foreach ($fields as $field) {
        $fields_val[$count++] = new \xmlrpcval($field, "string");
      }

      $msg = new \xmlrpcmsg('execute');
      $msg->addParam(new \xmlrpcval($this->database, "string"));
      $msg->addParam(new \xmlrpcval($this->id, "int"));
      $msg->addParam(new \xmlrpcval($this->password, "string"));
      $msg->addParam(new \xmlrpcval($relation, "string"));
      $msg->addParam(new \xmlrpcval("read", "string"));
      $msg->addParam(new \xmlrpcval($id_val, "array"));
      $msg->addParam(new \xmlrpcval($fields_val, "array"));

      $response = $client->send($msg);

       if ($response->faultCode()) {
        return -1;  
       } else {
        return $response->value();
      }
    }


     public function createOdooSupport($values, $relation) {

      $client = new \xmlrpc_client($this->server_url . "/xmlrpc/object");
      $client->setSSLVerifyPeer(0);

      $msg = new \xmlrpcmsg('execute');
      $msg->addParam(new \xmlrpcval($this->database, "string"));
      $msg->addParam(new \xmlrpcval($this->id, "int"));
      $msg->addParam(new \xmlrpcval($this->password, "string"));
      $msg->addParam(new \xmlrpcval($relation, "string"));
      $msg->addParam(new \xmlrpcval("create", "string"));
      $msg->addParam(new \xmlrpcval($values, "struct"));

      $response = $client->send($msg);

      if ($response->faultCode()) {
        return -1;
      } else {
        return $response->value();
      }
    }


    public function save() {

      $ids = $this->odooSearchSupport('name', '=', $this->typeSupport, 'crm.case.categ','string');
      $field_list = array('id');

      $id_odoo_category_array = $this->readOdooSupport($ids, $field_list, 'crm.case.categ');
      $id_odoo_category = $id_odoo_category_array[0][id];

      if  (empty($id_odoo_category)) {
        $values = array("name" => new \xmlrpcval('Help', "string"));
        $this->createOdooSupport($values, "crm.case.categ");

        $values = array("name" => new \xmlrpcval('Intervention', "string"));
        $this->createOdooSupport($values, "crm.case.categ");

        $values = array("name" => new \xmlrpcval('Invoice', "string"));
        $this->createOdooSupport($values, "crm.case.categ");

        $values = array("name" => new \xmlrpcval('Other', "string"));
        $this->createOdooSupport($values, "crm.case.categ");
      }

      $ids = $this->odooSearchByTwoCriteriaSupport('name', 'like', $this->nameOdoCustomer, 'res.partner','string',
                                                  'zip', '=', $this->zipOdooCustomer, 'string');

// **********************************
// read id customer odoo
// **********************************
      $field_list = array('id',
                          'email',
                        );
      $id_odoo_customer_array = $this->readOdooSupport($ids, $field_list, 'res.partner');
      $id_odoo_customer = $id_odoo_customer_array[0][id];
      $id_odoo_customer_email = $id_odoo_customer_array[0][email];

      if  (!empty($id_odoo_customer)) {

// **********************************
// Create Customer if doesn't exist in oddo
// **********************************
        $values = array(
                        "name" => new \xmlrpcval($this->supportSubject, "string"),
                        "partner_id" => new \xmlrpcval($id_odoo_customer, "int"),
                        "email_from" => new \xmlrpcval($id_odoo_customer_email, "string"),
                        "categ_id" => new \xmlrpcval($id_odoo_category, "int"),
                        "description" => new \xmlrpcval($this->messageSupport, "string"),
//                    "message_ids" => new \xmlrpcval(1, "int"),
        );

        $this->createOdooSupport($values, "crm.helpdesk");
      }
    }
  }
