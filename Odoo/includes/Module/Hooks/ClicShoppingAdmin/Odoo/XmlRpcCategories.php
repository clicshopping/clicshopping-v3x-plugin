<?php
/*
   * AdditionalCheckoutButtons.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\ClicShoppingAdmin\Odoo;

  use ClicShopping\OM\Registry;
  use ClicShopping\OM\HTML;
  use ClicShopping\OM\OSCOM;

  class XmlRpcCategories {

    public function __construct() {
      $OSCOM_Language = Registry::get('Language');
      $OSCOM_Db = Registry::get('Db');

      if (OSCOM::getSite() != 'ClicShoppingAdmin') {
        OSCOM::redirect('index.php', 'SSL');
      }

      $Qcategories = $OSCOM_Db->prepare('select cd.categories_name,
                                                 c.categories_id,
                                                 c.parent_id,
                                                 c.categories_image,
                                                 cd.categories_description,
                                                 cd.categories_head_title_tag,
                                                 cd.categories_head_desc_tag,
                                                 cd.categories_head_keywords_tag
                                         from :table_categories c,
                                              :table_categories_description cd
                                         where c.categories_id = :categories_id
                                         and c.categories_id = cd.categories_id
                                         and cd.language_id = :language_id
                                        ');
      $Qcategories->bindInt(':categories_id', $this->Id() );
      $Qcategories->bindInt(':language_id',  (int)$OSCOM_Language->getID() );
      $Qcategories->execute();

      $categories = $Qcategories->fetch();

      $this->data = $categories;
      $this->categories_name = $categories['categories_name'];
      $this->parent_id = $categories['parent_id'];
      $this->categories_image = $categories['categories_image'];
      $this->categories_description = $categories['categories_description'];
      $this->categories_head_title_tag = $categories['categories_head_title_tag'];
      $this->categories_head_desc_tag = $categories['categories_head_desc_tag'];
      $this->categories_head_keywords_tag = $categories['categories_head_keywords_tag'];
    }


    private function Id() {

      if (isset($_POST['categories_id'])) {
        $categories_id = HTML::sanitize($_POST['categories_id']);
      } else {
        $categories_id = HTML::sanitize($_GET['cID']);
      }

      return $categories_id;
    }

    private function getCategoriesImage() {
      if (file_exists(DIR_FS_CATALOG_IMAGES . $this->categories_image)) {
        $categories_image = DIR_FS_CATALOG_IMAGES . $this->categories_image;
        $categories_image = file_get_contents($categories_image);
        $categories_image = base64_encode($categories_image);
      }

      return $categories_image;
    }

    private function getCategoriesIdOdoo() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('clicshopping_categories_id', '=', $this->Id(), 'product.category', 'int',
                                                  'clicshopping_categories_parent_id', '=', $this->parent_id, 'int');

      $field_list = array('id');

      $Qcategories_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.category');
      $categories_id = $Qcategories_id[0][id];

      return $categories_id;
    }




    public function save() {
      $OSCOM_ODOO = Registry::get('Odoo');

      if  ($this->getCategoriesIdOdoo() == null || empty($this->getCategoriesIdOdoo())) {

// **********************************
// Create products if doesn't exist in odoo
// **********************************

        $values = array ("name" => new \xmlrpcval($this->categories_name, "string"),
                          "type"  => new \xmlrpcval('normal', "string"),
                          "complete_name"  => new \xmlrpcval($this->categories_name, "string"),
                          "clicshopping_categories_id" => new \xmlrpcval($this->Id(), "int"),
                          "ClicShopping_categories_save_to_catalog" => new \xmlrpcval(1, "int"),
                          "clicshopping_categories_parent_id" => new \xmlrpcval($this->parent_id, "int"),
                          "clicshopping_categories_image" => new \xmlrpcval($this->getCategoriesImage(), "string"),
                          "clicshopping_categories_description" => new \xmlrpcval($this->categories_description, "string"),
                          "clicshopping_categories_head_title_tag" => new \xmlrpcval($this->categories_head_title_tag, "string"),
                          "clicshopping_categories_head_desc_tag" => new \xmlrpcval($this->categories_head_desc_tag, "string"),
                          "clicshopping_categories_head_keywords_tag" => new \xmlrpcval($this->categories_head_keywords_tag, "string"),
                        );

        $OSCOM_ODOO->createOdoo($values, "product.category");


      }  else {

// **********************************
// update products if exist
// **********************************

        $id_list = array();
        $id_list[]= new \xmlrpcval($this->getCategoriesIdOdoo(), 'int');

        $values = array ("name" => new \xmlrpcval($this->categories_name, "string"),
                        "complete_name"  => new \xmlrpcval($this->categories_name, "string"),
                        "clicshopping_categories_id" => new \xmlrpcval($this->Id(), "int"),
                        "ClicShopping_categories_save_to_catalog" => new \xmlrpcval(1, "int"),
                        "clicshopping_categories_parent_id" => new \xmlrpcval($this->parent_id, "int"),
                        "clicshopping_categories_description" => new \xmlrpcval($this->categories_description, "string"),
                        "clicshopping_categories_head_title_tag" => new \xmlrpcval($this->categories_head_title_tag, "string"),
                        "clicshopping_categories_head_desc_tag" => new \xmlrpcval($this->categories_head_desc_tag, "string"),
                        "clicshopping_categories_head_keywords_tag" => new \xmlrpcval($this->categories_head_keywords_tag, "string"),
                        "clicshopping_categories_image" => new \xmlrpcval($this->getCategoriesImage(), "string"),
                      );

        $OSCOM_ODOO->updateOdoo($this->getCategoriesIdOdoo(), $values, 'product.category');
      }
    } // end save
  } //end class
