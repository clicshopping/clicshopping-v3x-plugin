<?php
/*
   * AdditionalCheckoutButtons.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\ClicShoppingAdmin\Odoo;

  use ClicShopping\OM\Registry;
  use ClicShopping\OM\HTML;
  use ClicShopping\OM\OSCOM;

  class XmlRpcCreateAccount {

    public function __construct() {

      if (OSCOM::getSite() != 'ClicShoppingAdmin') {
        OSCOM::redirect('index.php', 'SSL');
      }

      $this->customer_id =  $_POST['customer_id'];

      $this->customersNewsletter = 1;
      $this->customersFirstname = HTML::sanitize($_POST['customers_firstname']);
      $this->customersLastname = HTML::sanitize($_POST['customers_lastname']);
      $this->customersEmailAddress = HTML::sanitize($_POST['customers_email_address']);
      $this->customersTelephone = HTML::sanitize($_POST['customers_telephone']);
      $this->customersFax = HTML::sanitize($_POST['customers_fax']);
      $this->customersCellularPhone = HTML::sanitize($_POST['customers_cellular_phone']);
      $this->customersStreetAddress = HTML::sanitize($_POST['customers_street_address']);
      $this->customersSuburb = HTML::sanitize($_POST['customers_suburb']);
      $this->customersPostcode = HTML::sanitize($_POST['postcode']);
      $this->customersCity = HTML::sanitize($_POST['city']);

    }


    public function save() {

      $OSCOM_ODOO = Registry::get('Odoo');

// update newsletter
      if ($this->customersNewsletter == 1) $this->customersNewsletter = 0;

//country
      $OSCOM_ODOO->getCustomerId($this->customer_id);
      $CountryIdOdoo = $OSCOM_ODOO->getCountryIdOdoo();

// label to include in Odoo
      $OSCOM_ODOO->getLabelName('Customer');
      $labelId = $OSCOM_ODOO->getLabelId();
      $type_string = $OSCOM_ODOO->getLabelType();

// save
      $values = array (
                        "ref" => new xmlrpcval('WebStore - ' . $this->customer_id, "string"),
                        "phone" => new xmlrpcval($this->customersTelephone,"string"),
                        "mobile" => new xmlrpcval($this->customersCellularPhone,"string"),
                        "fax" => new xmlrpcval($this->customersFax,"string"),
                        "street" => new xmlrpcval($this->customersStreetAddress,"string"),
                        "street2"=> new xmlrpcval($this->customersSuburb,"string"),
                        "zip" => new xmlrpcval($this->customersPostcode,"string"),
                        "city" => new xmlrpcval($this->customersCity,"string"),
                        "comment" => new xmlrpcval('Website Registration - Admin Creation', "string"),
                        "name"    => new xmlrpcval( $this->customersLastname . ' ' . $this->customersFirstname, "string"),
                        "email"  => new xmlrpcval($this->customersEmailAddress, "string"),
                        "country_id" => new xmlrpcval($CountryIdOdoo,"int"),
                        "tz"      => new xmlrpcval("Europe/Paris", "string"),
                        "opt_out"  => new xmlrpcval($this->customersNewsletter, "double"),
                        "category_id" => new \xmlrpcval($labelId, $type_string),
                        "ClicShopping_customer_save_to_catalog" => new xmlrpcval(1, "int"),
                      );

      $OSCOM_ODOO->createOdoo($values, "res.partner");
    } // end save
  } //end class
