<?php
/*
 * XmlRpcOrder.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\ClicShoppingAdmin\Odoo;

  use ClicShopping\OM\Registry;
  use ClicShopping\OM\DateTime;
  use ClicShopping\OM\HTML;
  use ClicShopping\Sites\Common\HTMLOverrideCommon;
  use ClicShopping\OM\OSCOM;

  class XmlRpcOrder {
  
    public function __construct() {

      $OSCOM_Db = Registry::get('Db');
      $OSCOM_ODOO = Registry::get('Odoo');

      if (OSCOM::getSite() != 'ClicShoppingAdmin') {
        OSCOM::redirect('index.php', 'SSL');
      }

      $Qcustomers = $OSCOM_Db->prepare('select customers_id,
                                                orders_id,
                                                orders_status,
                                                date_purchased,
                                                orders_status_invoice,
                                                orders_date_finished,
                                                odoo_invoice,
                                                delivery_postcode,
                                                billing_postcode,
                                                provider_name_client,
                                                client_computer_ip,
                                                customers_group_id,
                                                customers_siret,
                                                customers_ape,
                                                customers_tva_intracom,
                                                customers_company,
                                                payment_method
                                           from :table_orders
                                           where orders_id = :orders_id
                                         ');
      $Qcustomers->bindValue(':orders_id', $this->getId());
      $Qcustomers->execute();

      $this->customersId = $Qcustomers->valueInt('customers_id');
      $this->orderId = $Qcustomers->valueInt('orders_id');
      $this->dateInvoice = $Qcustomers->value('date_purchased');
      $this->ordersStatusInvoice = $Qcustomers->valueInt('orders_status_invoice');
      $this->refWebstore = DateTime::getDateReferenceShort($this->dateInvoice) . 'S';
      $this->odooInvoice = $Qcustomers->valueInt('odoo_invoice');
      $this->ordersStatus = $Qcustomers->valueInt('orders_status');
      $this->deliveryPostcode= $Qcustomers->value('delivery_postcode');
      $this->billingPostcode = $Qcustomers->value('billing_postcode');
      $this->providerNameClient = $Qcustomers->value('provider_name_client');
      $this->clientComputerIp = $Qcustomers->value('client_computer_ip');
      $this->comment = $_POST['comments'];
      $this->customersGroupId = $Qcustomers->valueInt('customers_group_id');
      $this->customersSiret = $Qcustomers->value('customers_siret');
      $this->customersApe =  $Qcustomers->value('customers_ape');
      $this->customersTvaIntracom =  $Qcustomers->value('customers_tva_intracom');
      $this->customersCompany =  $Qcustomers->value('customers_company');
      $this->customersPaymentMethod =  $Qcustomers->value('payment_method');

      $this->companyId = $OSCOM_ODOO->getSearchCompanyIdOdoo();

    }


/**
 * Select the ID order of ClicShpping
 * @param string
 * @return  $this->orderId, id of order
 * @access private
 */
    private function getId() {

      if (is_numeric($_GET['oID']) ) {
        $order_id = HTML::sanitize($_GET['oID']);
      }

      return $order_id;
    }


    private function getPartnerId() {

      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_customers_id', '=', $this->customersId, 'res.partner');

      $field_list = array('id');

      $partner_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $partner_id = $partner_id[0][id];

      return $partner_id;
    }

    private function getPartnerInvoiceId() {

      $OSCOM_ODOO = Registry::get('Odoo');
      $OSCOM_Db = Registry::get('Db');

      $QcountryIdBillingOrder = $OSCOM_Db->prepare("select address_book_id
                                                     from :table_address_book
                                                     where customers_id = :customers_id
                                                     and  entry_postcode = :entry_postcode
                                                    ");
      $QcountryIdBillingOrder->bindInt(':customers_id', (int)$this->customersId );
      $QcountryIdBillingOrder->bindValue(':entry_postcode',$this->billingPostcode );
      $QcountryIdBillingOrder->execute();

      $country_id_billing_order = $QcountryIdBillingOrder->valueInt('address_book_id');

// Search odoo customer id / partner_invoice_id
      $ids = $OSCOM_ODOO->odooSearch('ref', '=', 'WebStore - ' . $country_id_billing_order, 'res.partner');

// read id order odoo partner_invoice_id
      $field_list = array('id');

      $partner_invoice_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $partner_invoice_id = $partner_invoice_id[0][id];

      return $partner_invoice_id;
    }

    private function getPartnerShippingId() { //null

      $OSCOM_ODOO = Registry::get('Odoo');
      $OSCOM_Db = Registry::get('Db');


      $QcountryIdShippingOrder = $OSCOM_Db->prepare('select address_book_id
                                                      from :table_address_book
                                                      where customers_id = :customers_id
                                                      and  entry_postcode = :entry_postcode
                                                     ');
      $QcountryIdShippingOrder->bindInt(':customers_id',  $this->customersId);
      $QcountryIdShippingOrder->bindValue(':entry_postcode', $this->deliveryPostcode );
      $QcountryIdShippingOrder->execute();

      $country_id_shipping_order_address_book_id = $QcountryIdShippingOrder->valueInt('address_book_id');

// Search odoo customer id / partner_shipping_id
      $ids = $OSCOM_ODOO->odooSearch('ref', '=', 'WebStore - ' . $country_id_shipping_order_address_book_id, 'res.partner');

// read id order odoo partner_invoice_id
      $field_list = array('id');

      $partner_shipping_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $partner_shipping_id = $partner_shipping_id[0][id];

      return $partner_shipping_id;
    }




    private function getOrderStatus() {

      if ($this->ordersStatus == 1) {
        $clicshopping_order_status = 'instance';
      } elseif ($this->ordersStatus == 2) {
        $clicshopping_order_status = 'processing';
      } elseif ($this->ordersStatus == 3) {
        $clicshopping_order_status = 'delivered';
      } elseif ($this->ordersStatus == 4) {
        $clicshopping_order_status = 'cancelled';
      } else {
        $clicshopping_order_status = 'instance';
      }

      return $clicshopping_order_status;
    }

    private function getInvoiceId() {

      $OSCOM_ODOO = Registry::get('Odoo');
// research order_id by origin order

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_order_reference', '=', $this->refWebstore . $this->getId(), 'sale.order');

// read id order odoo
      $field_list = array('id');

      $invoice_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'sale.order');
      $invoice_id = $invoice_id[0][id];

      return $invoice_id;
    }


    public function save() {

      $OSCOM_Db = Registry::get('Db');
      $OSCOM_ODOO = Registry::get('Odoo');
      $OSCOM_Order = Registry::get('Order');

      $discount_coupon_title = $OSCOM_ODOO->getDiscountCouponTitle($this->getId());
      $discount_coupon_amount = $OSCOM_ODOO->getDiscountCouponAmount($this->getId());
      $customer_discount_amount = $OSCOM_ODOO->getCustomerDiscountAmount($this->getId());
      $customer_discount_title = $OSCOM_ODOO->getCustomerDiscountTitle($this->getId());
      $shipping_title = $OSCOM_ODOO->getShippingTitle($this->getId());
      $shipping_amount = $OSCOM_ODOO->getShippingAmount($this->getId());

      if ($this->odooInvoice == 0) {
        if (is_null($this->getPartnerInvoiceId() )) $partner_invoice_id = $this->getPartnerId();

        if (is_null($this->getPartnerShippingId())) $partner_shipping_id = $this->getPartnerId();


// top of invoice
        $values = array ("partner_id" => new \xmlrpcval($this->getPartnerId(), "int"),
                          "partner_invoice_id" => new \xmlrpcval($partner_invoice_id, "int"),
                          "partner_shipping_id" => new \xmlrpcval($partner_shipping_id, "int"),
                          "company_id" => new \xmlrpcval($this->companyId, "int"),
                          "origin" => new \xmlrpcval("WebStore - order : " . $this->refWebstore . $this->getId(), "string"),
                          "client_order_ref" => new \xmlrpcval("WebStore - order : " . $this->refWebstore . $this->getId(), "string"),
                          "warehouse_id" => new \xmlrpcval($OSCOM_ODOO->getStockWharehouseId(), "int"),
                          "clicshopping_order_id" => new \xmlrpcval($this->orderId, "int"),
                          "clicshopping_order_reference" => new \xmlrpcval($this->refWebstore . $this->getId(), "string"),
                          "clicshopping_order_customer_id" => new \xmlrpcval($this->customersId, "int"),
                          "clicshopping_order_provider_name_client" => new \xmlrpcval($this->providerNameClient, "string"),
                          "clicshopping_order_client_computer_ip" => new \xmlrpcval($this->clientComputerIp, "string"),
                          "clicshopping_order_status" => new \xmlrpcval($this->getOrderStatus(), "string"),
                          "clicshopping_order_customers_group_id" => new \xmlrpcval($this->customersGroupId, "int"),
                          "clicshopping_order_customer_comments" => new \xmlrpcval($this->comment, "string"),
                          "clicshopping_order_customers_siret" => new \xmlrpcval($this->customersSiret, "string"),
                          "clicshopping_order_customers_ape" => new \xmlrpcval($this->customersApe, "string"),
                          "clicshopping_order_customers_company" => new \xmlrpcval($this->customersCompany, "string"),
                          "clicshopping_order_customers_tva_intracom" => new \xmlrpcval($this->customersTvaIntracom, "string"),
                          'clicshopping_order_payment_method' => new \xmlrpcval($this->customersPaymentMethod, "int"),
                      );

        $OSCOM_ODOO->createOdoo($values, "sale.order");

//order class in admin
        require_once('includes/classes/order.php');
        $order = new \order($this->getId());

// count number of product
        $count_products = sizeof($order->products);

        for ($o=0, $n=$count_products; $o<$n; $o++) {

//******************************************
// research invoice_id by id og clicshopping
//***************************************
// ====> mettre en relation avec la B2B
//          $ids = $OSCOM_ODOO->odooSearch('default_code', '=', $order->products[$o]['model'], 'product.template');
          $ids = $OSCOM_ODOO->odooSearch('clicshopping_products_id', '=', $order->products[$o]['products_id'], 'product.template');

// read id products odoo
          $field_list = array('id');

          $Qodoo_products_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
          $odoo_products_id = $Qodoo_products_id[0][id];

// **********************************
// name of products and options description
// doesn't take more than one options
// text limited  by odoo
// **********************************

          $products_name = $order->products[$o]['name'];

          if (isset($order->products[$o]['attributes']) && (sizeof($order->products[$o]['attributes']) > 0)) {
            for ($j = 0, $k = sizeof($order->products[$o]['attributes']); $j < $k; $j++) {
    // attributes reference
              if ($order->products[$o]['attributes'][$j]['reference'] != '' or $order->products[$o]['attributes'][$j]['reference'] !='null') {
                $attributes_reference = $order->products[$o]['attributes'][$j]['reference'] . ' - ';
              }
              $products_attributes = ' ' . $attributes_reference  . $order->products[$o]['attributes'][$j]['option'] . ': ' . $order->products[$o]['attributes'][$j]['value'];
              $products_attributes = HTMLOverrideCommon::stripHtmlTags($products_attributes);
            }
          }

          if ($products_attributes != '') {
            $products_attributes = ' / ' . $products_attributes;
          }

          $products_name_odoo = $products_name . $products_attributes;

// **********************************
// tax
// **********************************

// select the good tax in function the products
          $QProductsTax = $OSCOM_Db->prepare('select products_tax
                                               from :table_orders_products
                                               where orders_id = :orders_id
                                               and products_id = :products_id
                                             ');
          $QProductsTax->bindInt(':orders_id', (int)$this->getId());
          $QProductsTax->bindValue(':products_id', $order->products[$o]['products_id']);
          $QProductsTax->execute();

// select the tax code_tax_odoo in function the products tax
          $QProductsCodeTax = $OSCOM_Db->prepare('select code_tax_odoo
                                                   from :table_tax_rates
                                                   where tax_rate = :tax_rate
                                                 ');

          $QProductsCodeTax->bindValue(':tax_rate', $QProductsTax->value('products_tax'));
          $QProductsCodeTax->execute();

          $products_code_tax = $QProductsCodeTax->value('code_tax_odoo');

// research id tax by description


          if($products_code_tax != null) {
            $ids = $OSCOM_ODOO->odooSearch('description', '=', $products_code_tax, 'account.tax', 'string');
            $odoo_products_tax_id = $ids;
            $odoo_products_tax_id = $odoo_products_tax_id[0];

            if (!empty($odoo_products_tax_id)) {

              $type_tax_string = 'array';

              $tax = array(new \xmlrpcval(
                                          array(
                                                  new \xmlrpcval(6, "int"),// 6 : id link
                                                  new \xmlrpcval(0, "int"),
                                                  new \xmlrpcval(array(new \xmlrpcval($odoo_products_tax_id, "int")), "array")
                                                ), "array"
                                        )
                          );
            } else  {
              $tax = 0;
              $type_tax_string = 'int';
            }
          } else {
            $tax = 0;
            $type_tax_string = 'int';
          }
// Write a new line concerning the invoice
          $values = array (
                            "order_id" => new \xmlrpcval($this->getInvoiceId(), "int"),
                            "product_id" => new \xmlrpcval($odoo_products_id, "int"),
                            "company_id" => new \xmlrpcval($this->companyId, "int"),
                            "name" => new \xmlrpcval($products_name_odoo, "string"),
                            "price_unit" => new \xmlrpcval($order->products[$o]['final_price'],"double"),
                            "product_uom_qty" => new \xmlrpcval($order->products[$o]['qty'], 'double'),
                            "tax_id" =>  new \xmlrpcval($tax, $type_tax_string),
                          );

          $OSCOM_ODOO->createOdoo($values, "sale.order.line");
        } //end for

// Write a new line concerning the shipping by the service line available in odoo
        if ($shipping_amount != 0 ) {

          if (DISPLAY_DOUBLE_TAXE =='true') {
// select the good tax in function the products
            $QProductsTax = $OSCOM_Db->prepare('select products_tax
                                                   from :table_orders_products
                                                   where orders_id = :orders_id
                                                   and products_id = :products_id
                                                 ');
            $QProductsTax->bindInt(':orders_id', (int)$this->getId());
            $QProductsTax->bindValue(':products_id', $order->products[$o]['products_id']);
            $QProductsTax->execute();

            if  ( $QProductsTax->value('products_tax') != '' || $QProductsTax->value('products_tax') != 0) {
// select the tax code_tax_odoo in function the products tax
              $QProductsCodeTax = $OSCOM_Db->prepare('select code_tax_odoo
                                                         from :table_tax_rates
                                                         where tax_rate = :tax_rate
                                                       ');

              $QProductsCodeTax->bindValue(':tax_rate', $QProductsTax->value('products_tax'));

              $QProductsCodeTax->execute();

              $Qproducts_code_tax = $QProductsCodeTax->fetch();
              $products_code_tax = $Qproducts_code_tax['code_tax_odoo'];


              if($products_code_tax != null) {

                $ids = $OSCOM_ODOO->odooSearch('description', '=', $products_code_tax, 'account.tax', 'string');
                $odoo_products_tax_id = $ids;
                $odoo_products_tax_id = $odoo_products_tax_id[0];

                if (!empty($odoo_products_tax_id)) {

                  $type_tax_string = 'array';

                  $tax = array(new \xmlrpcval(
                                              array(
                                                      new \xmlrpcval(6, "int"),// 6 : id link
                                                      new \xmlrpcval(0, "int"),
                                                      new \xmlrpcval(array(new \xmlrpcval($odoo_products_tax_id, "int")), "array")
                                                    ), "array"
                                            )
                              );
                } else  {
                  $tax = 0;
                  $type_tax_string = 'int';
                }
              } else {
                $tax = 0;
                $type_tax_string = 'int';
              }
            }
          } else {
            $tax = 0;
            $type_tax_string = 'int';
          }

          $values = array (
                            "order_id" => new \xmlrpcval($this->getInvoiceId(), "int"),
                            "company_id" => new \xmlrpcval($this->companyId, "int"),
                            "product_id" => new \xmlrpcval(0, "int"),
                            "name" => new \xmlrpcval($shipping_title, "string"),
                            "price_unit" => new \xmlrpcval($shipping_amount,"double"),
                            "product_uom_qty" => new \xmlrpcval(1, 'int'),
                            "tax_id" =>  new \xmlrpcval($tax, $type_tax_string),
                          );

          $OSCOM_ODOO->createOdoo($values, "sale.order.line");

        }

// Write a new line concerning the customer discount  by the service line available in odoo
        if ($customer_discount_amount != 0 ) {
          $values = array (
                            "order_id" => new \xmlrpcval($this->getInvoiceId(), "int"),
                            "company_id" => new \xmlrpcval($this->companyId, "int"),
                            "product_id" => new \xmlrpcval(0, "int"),
                            "name" => new \xmlrpcval($customer_discount_title, "string"),
                            "price_unit" => new \xmlrpcval($customer_discount_amount,"double"),
                            "product_uom_qty" => new \xmlrpcval(1, 'int'),
                            "tax_id" =>  new \xmlrpcval(0, 'int'),
                          );
          $OSCOM_ODOO->createOdoo($values, "sale.order.line");
        }


// Write a new line concerning the discount coupon by the service line available in odoo
        if ($discount_coupon_amount != 0 ) {
          $values = array (
                            "order_id" => new \xmlrpcval($this->getInvoiceId(), "int"),
                            "company_id" => new \xmlrpcval($this->companyId, "int"),
                            "product_id" => new \xmlrpcval(0, "int"),
                            "name" => new \xmlrpcval($discount_coupon_title, "string"),
                            "price_unit" => new \xmlrpcval($discount_coupon_amount,"double"),
                            "product_uom_qty" => new \xmlrpcval(1, 'int'),
                            "tax_id" =>  new \xmlrpcval(0, 'int'),
                          );

          $OSCOM_ODOO->createOdoo($values, "sale.order.line");
        }

        if ($this->getInvoiceId() != '') {
// Update ClicShopping
          $Qupdate = $OSCOM_Db->prepare('update :table_orders
                                          set odoo_invoice = :odoo_invoice
                                          where orders_id = :orders_id
                                        ');
          $Qupdate->bindInt(':orders_id', $this->getId());
          $Qupdate->bindValue(':odoo_invoice', 1);
          $Qupdate->execute();
        }

        $OSCOM_ODOO->workflowOdoo('sale.order', 'order_confirm',  $this->getInvoiceId());

      }
    }
  } // end $this->odooInvoice