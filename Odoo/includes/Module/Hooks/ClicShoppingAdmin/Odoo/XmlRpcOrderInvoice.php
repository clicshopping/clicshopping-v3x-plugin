<?php
/*
 * xml_rpc_admin_order_invoice.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\ClicShoppingAdmin\Odoo;

  use ClicShopping\OM\DateTime;
  use ClicShopping\OM\HTML;
  use ClicShopping\OM\Registry;
  use ClicShopping\OM\OSCOM;

  class XmlRpcOrderInvoice {

    public function __construct() {

      $OSCOM_Db = Registry::get('Db');
      $OSCOM_ODOO = Registry::get('Odoo');

      if (OSCOM::getSite() != 'ClicShoppingAdmin') {
        OSCOM::redirect('index.php', 'SSL');
      }

      $Qcustomers = $OSCOM_Db->prepare('select customers_id,
                                               orders_status,
                                               date_purchased,
                                               orders_status_invoice,
                                               orders_date_finished,
                                               odoo_invoice
                                         from :table_orders
                                         where orders_id = :orders_id
                                       ');
      $Qcustomers->bindValue(':orders_id', $this->getId());
      $Qcustomers->execute();

      $this->customersId = $Qcustomers->valueInt('customers_id');
      $this->dateInvoice = $Qcustomers->value('date_purchased');
      $this->webstore = DateTime::getDateReferenceShort($this->dateInvoice) . 'S'; // ref invoice  clicshopping
      $this->odooInvoice = $Qcustomers->valueInt('odoo_invoice'); // status of Odoo in clicshopping
      $this->status = $_POST['status'];
      $this->customersGroupId = $Qcustomers->valueInt('customers_group_id');

      $this->getCompanyId = $OSCOM_ODOO->getSearchCompanyIdOdoo();
    }

/**
 * Select the ID order of ClicShpping
 * @param string
 * @return  $this->orderId, id of order
 * @access private
 */
    private function getId() {

      if (is_numeric($_GET['oID']) ) {
        $order_id = HTML::sanitize((int)$_GET['oID']);
      }

      return $order_id;
    }


    private function getOdooOrderId() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_order_id', '=',  $this->getId(), 'sale.order');

      $field_list = array('id');

      $odoo_order_read = $OSCOM_ODOO->readOdoo($ids, $field_list, 'sale.order');
      $odoo_order_id = $odoo_order_read[0][id];

      return $odoo_order_id;
    }

    private function getOdooOrderName() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_order_id', '=',  $this->getId(), 'sale.order');
    
      $field_list = array('name');
    
      $odoo_order_read = $OSCOM_ODOO->readOdoo($ids, $field_list, 'sale.order');

      $odoo_order_name = $odoo_order_read[0][name];
      
      return $odoo_order_name;
    }


// invoice
    private function getInvoiceId() {
      $OSCOM_ODOO = Registry::get('Odoo');

      if (!is_null($this->getOdooOrderName())) {
        $ids = $OSCOM_ODOO->odooSearch('origin', '=', $this->getOdooOrderName(), 'account.invoice');
      } else {
        $ids = $OSCOM_ODOO->odooSearch('origin', '=', $this->webstore .  $this->getId(), 'account.invoice');
      }

      $field_list = array('id');
      $qInvoiceId = $OSCOM_ODOO->readOdoo($ids, $field_list, 'account.invoice');

      $invoice_id = $qInvoiceId[0][id];

      return $invoice_id;
    }

    private function getInvoiceShippingLineId() {
      $OSCOM_ODOO = Registry::get('Odoo');



      $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('name','=', $OSCOM_ODOO->getShippingTitle($this->getId()), 'account.invoice.line', 'string',
                                                  'invoice_id', '=', $this->getInvoiceId(), 'int');

      $field_list = array('id',
                          'invoice_id',
                          'account_id',
                          );

      $qInvoiceShippingLine = $OSCOM_ODOO->readOdoo ($ids, $field_list, 'account.invoice.line');

      $invoice_shipping_line_id = $qInvoiceShippingLine[0][id]; //15

      return $invoice_shipping_line_id;
    }

    private function getInvoiceShippingLineName() {
      $OSCOM_ODOO = Registry::get('Odoo');
      $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('name','=', $OSCOM_ODOO->getShippingTitle($this->getId()), 'account.invoice.line', 'string',
                                                  'invoice_id', '=', $this->getInvoiceId(), 'int');

      $field_list = array('id',
                          'name',
                         );

      $qInvoiceShippingLine = $OSCOM_ODOO->readOdoo ($ids, $field_list, 'account.invoice.line');

      $invoice_shipping_line_name = $qInvoiceShippingLine[0][name];

      return $invoice_shipping_line_name;
    }




//customer discount
    private function getInvoiceCustomerDiscountLineId() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('name','=', $OSCOM_ODOO->getCustomerDiscountTitle($this->getId()), 'account.invoice.line', 'string',
                                                   'invoice_id', '=', $this->getInvoiceId(), 'int');

      $field_list = array('id');

      $QinvoiceDiscountLine = $OSCOM_ODOO->readOdoo ($ids, $field_list, 'account.invoice.line');
      $invoice_discount_line_id = $QinvoiceDiscountLine[0][id]; //26

      return $invoice_discount_line_id;
    }


    private function getInvoiceCustomerDiscountLineName() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('name','=', $OSCOM_ODOO->getCustomerDiscountTitle($this->getId()), 'account.invoice.line', 'string',
                                                 'invoice_id', '=', $this->getInvoiceId(), 'int');

      $field_list = array('name');

      $QinvoiceDiscountLine = $OSCOM_ODOO->readOdoo ($ids, $field_list, 'account.invoice.line');

      $invoice_discount_line_name = $QinvoiceDiscountLine[0][name];

      return $invoice_discount_line_name;
    }


// coupon
    private function getInvoiceCouponLineId() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('name','=', $OSCOM_ODOO->getDiscountCouponTitle($this->getId()), 'account.invoice.line', 'string',
                                                  'invoice_id', '=', $this->getInvoiceId(), 'int');

      $field_list = array('id');

      $invoice_coupon_line = $OSCOM_ODOO->readOdoo($ids, $field_list, 'account.invoice.line');
      $invoice_coupon_line_id = $invoice_coupon_line[0][id];

      return $invoice_coupon_line_id;
    }
    
    
    private function getInvoiceCouponLineName() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('name','=',   $OSCOM_ODOO->getDiscountCouponTitle($this->getId()), 'account.invoice.line', 'string',
                                                  'invoice_id', '=', $this->getInvoiceId(), 'int');

      $field_list = array('name');

      $invoice_coupon_line = $OSCOM_ODOO->readOdoo($ids, $field_list, 'account.invoice.line');

      $invoice_coupon_line_name = $invoice_coupon_line[0][name];

      return $invoice_coupon_line_name;
    }


    private function stockMoveOdoo() {
      $OSCOM_ODOO = Registry::get('Odoo');

// Stock.move, create the good real stock and not the virtual
      if ( ($this->odooInvoice == '3')  || ($this->odooInvoice == '4') ) {
// search id of order in Odoo for stock move
        $ids = $OSCOM_ODOO->odooSearchAll('origin', '=', $this->getOdooOrderName(), 'stock.move');

        $field_list = array('id');

        $Qodoo_order_read = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.move');
//        $odoo_picking_id = $Qodoo_order_read[0][id];

        if ($this->odooInvoice = 3) {
          foreach ($Qodoo_order_read as $key) {
            $odoo_picking_id = $key[id];
            $OSCOM_ODOO->buttonClickOdoo('stock.move', 'action_done', $odoo_picking_id);
          }
        } else {
          foreach ($Qodoo_order_read as $key) {
            $odoo_picking_id = $key[id];
            $OSCOM_ODOO->buttonClickOdoo('stock.move', 'action_cancel', $odoo_picking_id);
          }
        }
      }
    }

    public function save() {
      $OSCOM_ODOO = Registry::get('Odoo');
      $OSCOM_Db = Registry::get('Db');

      if ($this->odooInvoice > 0) {

// workflow to update the Odoo invoice
// transfert order to invoice before treatment
        if ($this->status == 2) {
          $OSCOM_ODOO->workflowOdoo('sale.order', 'manual_invoice',  $this->getOdooOrderId());
        }


// shipping line and update
        if ($this->getInvoiceId() != null ) {
          $values = array("invoice_id" => new \xmlrpcval($this->getInvoiceId(), "int"),
                          "company_id" => new \xmlrpcval($this->getCompanyId, "int"),
                          "name" => new \xmlrpcval($this->getInvoiceShippingLineName(), "string"),
                          "account_id" => new \xmlrpcval($OSCOM_ODOO->getShippingAccountingOdooId(), "int"),
                         );

          $OSCOM_ODOO->updateOdoo($this->getInvoiceShippingLineId(), $values, 'account.invoice.line');

// Coupon discount
          $values = array(
                          "id" => new \xmlrpcval($this->getInvoiceCouponLineId(), "int"),
                          "invoice_id" => new \xmlrpcval($this->getInvoiceId(), "int"), // 13
                          "company_id" => new \xmlrpcval($this->getCompanyId, "int"),
                          "name" => new \xmlrpcval($this->getInvoiceCouponLineName(), "string"),
                          "account_id" => new \xmlrpcval($OSCOM_ODOO->getDiscountAccountingOdooId(), "int"),
                        );

          $OSCOM_ODOO->updateOdoo($this->getInvoiceCouponLineId(), $values, 'account.invoice.line');

// discount customer
          $values = array(
                          "id" => new \xmlrpcval($this->getInvoiceCustomerDiscountLineId(), "int"), // 26
                          "invoice_id" => new \xmlrpcval($this->getInvoiceId(), "int"), // 13
                          "company_id" => new \xmlrpcval($this->getCompanyId, "int"),
                          "name" => new \xmlrpcval($this->getInvoiceCustomerDiscountLineName(), "string"),
                          "account_id" => new \xmlrpcval($OSCOM_ODOO->getDiscountAccountingOdooId(), "int"),
                        );

          $OSCOM_ODOO->updateOdoo($this->getInvoiceCustomerDiscountLineId(), $values, 'account.invoice.line');

          if ($this->status == '2'  && $this->odooInvoice == '1' )  {
            $this->odooInvoice = 2;
          } elseif ($this->status == '3' && $this->odooInvoice == '2')  {
            $OSCOM_ODOO->workflowOdoo('account.invoice', 'invoice_open',  $this->getInvoiceId());
            $this->odooInvoice = 3;
          } elseif ($this->status == '4' &&  $this->odooInvoice = '2') {
            $OSCOM_ODOO->workflowOdoo ('account.invoice', 'invoice_cancel', $this->getInvoiceId());
            $this->odooInvoice = 4;
          }
        }

// Stock Management
        $this->stockMoveOdoo();

// Update ClicShopping
        $Qupdate = $OSCOM_Db->prepare('update :table_orders
                                        set odoo_invoice = :odoo_invoice
                                        where orders_id = :orders_id
                                      ');
        $Qupdate->bindInt(':orders_id', (int)$this->getId());
        $Qupdate->bindValue(':odoo_invoice', $this->odooInvoice);
        $Qupdate->execute();

      }
    }
  }
