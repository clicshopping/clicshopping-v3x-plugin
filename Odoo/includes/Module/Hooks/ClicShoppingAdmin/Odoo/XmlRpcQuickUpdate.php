<?php
/*
 * XmlRpcQuickUpdate.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\ClicShoppingAdmin\Odoo;

  use ClicShopping\OM\Registry;
  use ClicShopping\OM\HTML;
  use ClicShopping\OM\OSCOM;

  class XmlRpcQuickUpdate {

    public function __construct() {

      $OSCOM_Language = Registry::get('Language');
      $OSCOM_Db = Registry::get('Db');

      if (OSCOM::getSite() != 'ClicShoppingAdmin') {
        OSCOM::redirect('index.php', 'SSL');
      }

      $Qproduct = $OSCOM_Db->prepare('select distinct pd.products_name,
                                                      p.products_model,
                                                      p.products_price,
                                                      p.products_weight,
                                                      p.products_status,
                                                      p.products_quantity,
                                                      p.products_tax_class_id,
                                                      p.suppliers_id,
                                                      p.manufacturers_id,
                                                      p.products_price_comparison,
                                                      p.products_min_qty_order,
                                                      p.products_only_online,
                                                      p.products_id
                                               from :table_products p,
                                                    :table_products_description pd
                                               where p.products_id = :products_id
                                               and pd.products_id = p.products_id
                                               and pd.language_id = :language_id
                                              ');

      $Qproduct->bindInt(':products_id',  (int)$this->Id());
      $Qproduct->bindInt(':language_id',  (int)$OSCOM_Language->getID() );
      $Qproduct->execute();


      $this->productsName  = $Qproduct->value('products_name');
      $this->productsModel  = $Qproduct->value('products_model');
      $this->productsPrice  = $Qproduct->value('products_price');
      $this->productsWeight  = $Qproduct->value('products_weight');
      $this->productsStatus  = $Qproduct->value('products_status');
      $this->productQuantity = $Qproduct->value('products_quantity');
      $this->productsTaxClassId = $Qproduct->value('products_tax_class_id');
      $this->suppliersId = $Qproduct->value('suppliers_id');
      $this->productManufacturersId = $Qproduct->value('manufacturers_id');
      $this->productsPriceComparison = $Qproduct->value('products_price_comparison');
      $this->productsMinQtyOrder = $Qproduct->value('products_min_qty_order');
      $this->productsOnlyOnline = $Qproduct->value('products_only_online');
      $this->productsIdClicshopping  = $Qproduct->value('products_id');

      $this->productsNameOdoo = '[' . $this->productsModel . '] ' . $this->productsName;

      $this->date = date("Y-m-d H:i:s");

    }


/**
 * Select the ID Product of ClicShpping
 * @param string
 * @return  $products_id, id of product
 * @access private
 */
    private function Id() {

      if (isset($_POST['id'])) {
        $id = HTML::sanitize($_POST['id']);
      }
      return $id;
    }


/**
 * Select the ID of company in Odoo
 * @param string
 * @return  $company_id, id of company odoo
 * @access public
 */
    private function getCompanyId() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $company_id = $OSCOM_ODOO->getSearchCompanyIdOdoo();

      return $company_id;
    }

/**
 * Select the ID Product in Odoo
 * @param string
 * @return  $products_id_odoo, id of product odoo
 * @access private
 */
    private function getProductsIdOdoo() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_products_id', '=', $this->productsIdClicshopping, 'product.template', 'int');
      $field_list = array('id');

      $Qproducts_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
      $products_id_odoo = $Qproducts_id[0][id];


      return $products_id_odoo;
    }

/**
 * Select the ID of the Odoo brand
 * @param string
 * @return  $manufacturer_id_odoo, id of the brand
 * @access private
 */
    private function getManufacturerId() {
      $OSCOM_ODOO = Registry::get('Odoo');

      if (!empty($this->productManufacturersId)) {

        $ids = $OSCOM_ODOO->odooSearch('clicshopping_manufacturers_id', '=', $this->productManufacturersId, 'clicshopping.manufacturer', 'int');

        $field_list = array('clicshopping_manufacturers_id');
//        $field_list = array('id');
        $Qmanufacturers_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'clicshopping.manufacturer');
        $manufacturer_id_odoo = $Qmanufacturers_id_odoo[0][id];
      }

      return $manufacturer_id_odoo;
    }

    private function getSupplier() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_suppliers_id', '=', $this->suppliersId , 'res.partner', 'int');

      $field_list = array('id',
                          'name',
                        );

      $Qsuppliers_id_name_res_partner_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $suppliers_id_name_res_partner_odoo = $Qsuppliers_id_name_res_partner_odoo[0][id];


      if  (!empty($suppliers_id_name_res_partner_odoo)) {

        $ids =  $OSCOM_ODOO->odooSearchByTwoCriteria('product_code', '=', $this->productsModel, 'product.supplierinfo', 'string',
                                                     'product_tmpl_id', '=', $this->getProductsIdOdoo(), 'int');

        $field_list = array('id',
                            'name',
                          );

        $Qsuppliers_id_name_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.supplierinfo');
        $suppliers_id_name_odoo = $Qsuppliers_id_name_odoo[0][id];

        if  (empty($suppliers_id_name_odoo) ) {

          $values = array (  "name" => new xmlrpcval($suppliers_id_name_res_partner_odoo, "int"),
                            "product_name" => new xmlrpcval($this->productsName, "string"),
                            "product_code" => new xmlrpcval($this->productsModel, "string"),
                            "product_tmpl_id" => new xmlrpcval($this->getProductsIdOdoo(), "int"),
                          );

          $OSCOM_ODOO->createOdoo($values, "product.supplierinfo");

        } else {

          $id_list = array();
          $id_list[] = new xmlrpcval($suppliers_id_name_odoo, 'int');

          $values = array ( "name" => new xmlrpcval($suppliers_id_name_res_partner_odoo, "int"),
                            "product_name" => new xmlrpcval($this->productsName, "string"),
                            "product_code" => new xmlrpcval($this->productsModel,"string"),
                            "product_tmpl_id" => new xmlrpcval($this->getProductsIdOdoo(), "int"),
                          );

          $OSCOM_ODOO->updateOdoo($suppliers_id_name_odoo, $values, 'product.supplierinfo');
        }

      } else {

        $values = array("name" => new xmlrpcval($suppliers_id_name_res_partner_odoo, "int"),
                        "product_name" => new xmlrpcval($this->productsName, "string"),
                        "product_code" => new xmlrpcval($this->productsModel, "string"),
                        "product_tmpl_id" => new xmlrpcval($this->getProductsIdOdoo(), "int"),
                      );

        $OSCOM_ODOO->createOdoo($values, "product.supplierinfo");

      }
    }

// wharehouse

    private function getStockWharehouseCode() {
      $OSCOM_ODOO = Registry::get('Odoo');

// stock warehouse search id and code concerning ClicShopping Wharehouse
      $ids = $OSCOM_ODOO->odooSearch('name', '=', $OSCOM_ODOO->getWarehouseName(), 'stock.warehouse');

      $field_list = array('code');

      $Qstock_wharehouse = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.warehouse');

      $stock_wharehouse_code = $Qstock_wharehouse[0][code];

      return $stock_wharehouse_code;
    }
/*
    private function getStockWharehouseId() {
      $OSCOM_ODOO = Registry::get('Odoo');

// stock warehouse search id and code concerning ClicShopping Wharehouse
      $ids = $OSCOM_ODOO->odooSearch('name', '=', 'ClicShopping', 'stock.warehouse');

      $field_list = array('id');

      $Qstock_wharehouse = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.warehouse');

      $stock_wharehouse_id = $Qstock_wharehouse[0][id];

      return $stock_wharehouse_id;
    }
*/
    private function getWharehouseLocationId() {
      $OSCOM_ODOO = Registry::get('Odoo');
      $ids = $OSCOM_ODOO->odooSearch('name', '=', $this->getStockWharehouseCode(), 'stock.location');

      $field_list = array('id');

      $Qstock_location = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.location');
      $stock_location_id = $Qstock_location[0][id];

      return $stock_location_id;
    }

    private function getWharehouseLocationName() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('name', '=', $this->getStockWharehouseCode(), 'stock.location');

      $field_list = array('name');

      $Qstock_location = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.location');

      $stock_location_name = $Qstock_location[0][name];

      return $stock_location_name;
    }

// stock
    private function getStockAvailabe() {

      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('name', '=', $this->getStockWharehouseCode(), 'stock.location');

      $field_list = array('qty_available');

      $Qproducts_stock_available = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
      $products_stock_available = $Qproducts_stock_available[0][qty_available];

      return $products_stock_available;
    }




    private function getStock() {

      $OSCOM_ODOO = Registry::get('Odoo');

      $products_name_odoo = '[' . $this->productsModel . '] ' . $this->productName;

      if (!empty($this->getWharehouseLocationId())) {

        if ($this->productQuantity != $this->getStockAvailabe() ) {

// search qty in stock.quant
          $ids = $OSCOM_ODOO->odooSearchAllByTwoCriteria('product_id', '=', $products_name_odoo, 'stock.quant', 'string',
                                                         'location_id', 'like', $this->getWharehouseLocationName() . '%', 'string');

          $field_list = array('qty');

          $QSearchStock = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.quant');

          $stock_odoo_qty_total = '0';

// Stock calcul inside wharehouse
          foreach ($QSearchStock as $key) {
            $products_stock_qty = $key[qty];
            $stock_odoo_qty_total = $stock_odoo_qty_total + $products_stock_qty;
          }

// difference between ClicShopping and Odoo
          $new_stock = $this->productQuantity - $stock_odoo_qty_total;

// hack, don't find the good value in location.stock. Add +1 on id because it's create just after
          if (ODOO_WEB_SERVICE_COMPANY_WEB_SERVICE != 'Your Company') {
            $stock_location_id = $this->getWharehouseLocationId() + 1;
          } else {
            $stock_location_id = $this->getWharehouseLocationId();
          }

// move stock

          if ($new_stock > 0) {
            $values = array("product_id" => new \xmlrpcval($this->setProductsIdOdoo(), "int"),
                            "product_uom_qty" => new \xmlrpcval($new_stock, "double"), // qty of my product
                            "name" => new \xmlrpcval($products_name_odoo, "string"),
                            "invoice_state" => new \xmlrpcval('none', "string"),
                            "date" => new \xmlrpcval($this->date, "string"),
                            "date_expected" => new \xmlrpcval($this->date, "string"),
                            "company_id" => new \xmlrpcval($this->getCompanyId(), "int"),
                            "procure_method" => new \xmlrpcval('make_to_stock', "string"),
                            "location_dest_id" => new \xmlrpcval($stock_location_id, "string"),
                            "location_id" => new \xmlrpcval(5, "int"), // Virtual Locations/Inventory loss
                            "product_uom" => new \xmlrpcval(1, "int"),
                            "origin" => new \xmlrpcval("ClicShopping Webstore", "string"),
                          );
          } else {

// Adapted for odoo on the calcul
          $new_stock = $new_stock * (-1);

          $values = array(  "product_id" => new \xmlrpcval($this->setProductsIdOdoo(), "int"),
                            "product_uom_qty" => new \xmlrpcval($new_stock, "double"), // qty of my product
                            "name" => new \xmlrpcval($products_name_odoo, "string"),
                            "invoice_state" => new \xmlrpcval('none', "string"),
                            "date" => new \xmlrpcval($this->date, "string"),
                            "date_expected" => new \xmlrpcval($this->date, "string"),
                            "company_id" => new \xmlrpcval($this->getCompanyId(), "int"),
                            "procure_method" => new \xmlrpcval('make_to_stock', "string"),
                            "location_dest_id" => new \xmlrpcval(5, "string"),
                            "location_id" => new \xmlrpcval($stock_location_id, "int"), // Virtual Locations/Inventory loss
                            "product_uom" => new \xmlrpcval(1, "int"),
                            "origin" => new \xmlrpcval("ClicShopping Webstore", "string"),
                            );
          }

          $OSCOM_ODOO->createOdoo($values, "stock.move");


// search id for stock.move concerning the product
          $ids = $OSCOM_ODOO->odooSearchAllByTwoCriteria('product_id', '=', $this->setProductsIdOdoo(), 'stock.move', 'int',
                                                         'date', '=', $this->date, 'string');

          $field_list = array('id');

          $Qodoo_move_read = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.move');
          $odoo_picking_id = $Qodoo_move_read[0][id];

          $OSCOM_ODOO->buttonClickOdoo('stock.move', 'action_done', $odoo_picking_id);
        }
      } // end !empty($stock_location_id)
    }

/**
 * create or update Odoo article
 * @param string
 * @return
 * @access public
 */
    public function save() {
      $OSCOM_ODOO = Registry::get('Odoo');
      $OSCOM_Db = Registry::get('dB');

// Search odoo products tax
      $QProductsCodeTax = $OSCOM_Db->prepare('select distinct tax_class_id,
                                                                code_tax_odoo
                                                 from :table_tax_rates
                                                 where tax_class_id = :tax_class_id
                                               ');

      $QProductsCodeTax->bindValue(':tax_class_id', $this->productsTaxClassId);

      $QProductsCodeTax->execute();

      $Qproducts_code_tax = $QProductsCodeTax->fetch();
      $products_code_tax = $Qproducts_code_tax['code_tax_odoo'];

// research id tax by description
      if (!empty($products_code_tax)) {
        $ids = $OSCOM_ODOO->odooSearch('description', '=', $products_code_tax, 'account.tax', 'string');
        $odoo_products_tax_id = $ids;
        $odoo_products_tax_id = $odoo_products_tax_id[0];

        if (!empty($odoo_products_tax_id)) {

          $type_tax_string = 'array';

          $tax = array(new xmlrpcval(
                                      array(
                                            new xmlrpcval(6, "int"),// 6 : id link
                                            new xmlrpcval(0, "int"),
                                            new xmlrpcval(array(new xmlrpcval($odoo_products_tax_id, "int")), "array")
                                          ), "array"
                                    )
                      );
        } else  {
          $tax = 0;
          $type_tax_string = 'int';
        }

      } else {
        $tax = 0;
        $type_tax_string = 'int';
      }



      if  (!empty($this->getProductsIdOdoo())) {
        $id_list = array();
        $id_list[]= new xmlrpcval($this->getProductsIdOdoo(), 'int');

        $values = array ( "default_code" => new xmlrpcval($this->productsModel, "string"),
                          "name" => new xmlrpcval($this->productsName,"string"),
                          "description_sale" => new xmlrpcval($this->productsName,"string"),
                          "list_price" => new xmlrpcval($this->productsPrice,"double"),
                          "weight" => new xmlrpcval($this->productsWeight,"double"),
                          "clicshopping_products_price_comparison" => new xmlrpcval($this->productsPriceComparison,"double"),
                          "clicshopping_products_only_online" => new xmlrpcval($this->productsOnlyOnline,"double"),
                          "clicshopping_products_stock_status" => new xmlrpcval($this->productsStatus,"double"),
                          "clicshopping_products_min_qty_order" => new xmlrpcval($this->productsMinQtyOrder,"double"),
                          "taxes_id" =>  new xmlrpcval($tax, $type_tax_string),
                          "clicshopping_product_manufacturer_id" => new xmlrpcval($this->productManufacturersId,"int"),
                        );

        $OSCOM_ODOO->updateOdoo($this->getProductsIdOdoo(), $values, 'product.template');
      }

      if (!empty($this->suppliersId) || $this->productQuantity == null) {
        $this->getSupplier();
      }

      if (!empty($this->productQuantity) ||  $this->productQuantity == null) {
        $this->getStock();
      }
    }
 }

