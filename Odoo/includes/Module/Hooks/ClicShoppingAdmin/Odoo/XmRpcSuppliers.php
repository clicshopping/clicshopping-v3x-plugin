<?php
/*
 * XmlRpcSuppliers.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  namespace ClicShopping\OM\Module\Hooks\ClicShoppingAdmin\Odoo;

  use ClicShopping\OM\Registry;
  use ClicShopping\OM\HTML;
  use ClicShopping\OM\OSCOM;

  class XmlRpcSuppliers {

    public function __construct() {

      if (OSCOM::getSite() != 'ClicShoppingAdmin') {
        OSCOM::redirect('index.php', 'SSL');
      }

      $this->suppliers_id = HTML::sanitize($_POST['suppliers_id']);
      $this->suppliers_name = HTML::sanitize($_POST['suppliers_name']);
      $this->suppliers_manager = HTML::sanitize($_POST['suppliers_manager']);
      $this->suppliers_email_address = HTML::sanitize($_POST['suppliers_email_address']);
      $this->suppliers_phone = HTML::sanitize($_POST['suppliers_phone']);
      $this->suppliers_fax = HTML::sanitize($_POST['suppliers_fax']);
      $this->suppliers_address = HTML::sanitize($_POST['suppliers_address']);
      $this->suppliers_suburb = HTML::sanitize($_POST['suppliers_suburb']);
      $this->suppliers_postcode = HTML::sanitize($_POST['suppliers_postcode']);
      $this->suppliers_city = HTML::sanitize($_POST['suppliers_city']);
      $this->suppliers_notes = HTML::sanitize($_POST['suppliers_notes']);
    }


//************************************************
// Country Code
//************************************************
    private function getCountrySupplierId() {
      $OSCOM_Db = Registry::get('Db');

      $QcountryIdCustomer = $OSCOM_Db->prepare("select suppliers_country_id
                                                 from :table_suppliers
                                                 where suppliers_id = :suppliers_id
                                                ");
      $QcountryIdCustomer->bindInt(':suppliers_id', $this->suppliers_id);
      $QcountryIdCustomer->execute();

      $country_id_supplier =  $QcountryIdCustomer->valueInt('suppliers_country_id');

      return $country_id_supplier;
    }

    private function getCountryCode() {

      $OSCOM_Db = Registry::get('Db');

      $QcountryCode = $OSCOM_Db->prepare("select countries_iso_code_2
                                           from :table_countries
                                           where countries_id = :countries_id
                                          ");
      $QcountryCode->bindInt(':countries_id',$this->getCountrySupplierId());
      $QcountryCode->execute();

      $country_code = $QcountryCode->value('countries_iso_code_2');

      return $country_code;
    }


    private function getCountryIdOdoo() {

      $OSCOM_ODOO = Registry::get('Odoo');

// odoo
      $ids = $OSCOM_ODOO->odooSearch('code', '=', $this->getCountryCode(), 'res.country');

      $field_list = array('id',
                          'name',
                        );

      $Qcountry_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.country');
      $country_id_odoo = $Qcountry_id_odoo[0][id];

      return $country_id_odoo;
    }

    private function getIdOdooSupplier() {
      $OSCOM_ODOO = Registry::get('Odoo');

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_suppliers_id', '=', $this->suppliers_id, 'res.partner');
    
      $field_list = array('id');
    
      $id_odoo_suppplier_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $id_odoo_supplier = $id_odoo_suppplier_array[0][id];

      return $id_odoo_supplier;
    }

    public function save() {

      $OSCOM_ODOO = Registry::get('Odoo');

// label to include in Odoo
      $OSCOM_ODOO->getLabelName('Supplier');
      $labelId = $OSCOM_ODOO->getLabelId();
      $type_string = $OSCOM_ODOO->getLabelType();


      if  ($this->getIdOdooSupplier() == null || empty($this->getIdOdooSupplier())) {

// **********************************
// Create Customer if doesn't exist in oddo
// **********************************
        $values = array("ref" => new \xmlrpcval('WebStore Suppliers - ' . $this->suppliers_id, "string"),
                        "name"    => new \xmlrpcval($this->suppliers_name . ' ' . $this->suppliers_manager, "string"),
                        "email"  => new \xmlrpcval($this->suppliers_email_address, "string"),
                        'phone' => new \xmlrpcval($this->suppliers_phone,"string"),
                        'fax' => new \xmlrpcval($this->suppliers_fax,"string"),
                        'street' => new \xmlrpcval($this->suppliers_address,"string"),
                        'street2'=>new \xmlrpcval($this->suppliers_suburb,"string"),
                        'zip' => new \xmlrpcval($this->suppliers_postcode,"string"),
                        'city' => new \xmlrpcval($this->suppliers_city,"string"),
                        'country_id' => new \xmlrpcval($this->getCountryIdOdoo(),"int"),
                        "supplier" => new \xmlrpcval(1,"int"),
                        "tz"      => new \xmlrpcval("Europe/Paris", "string"),
                        "supplier" => new \xmlrpcval(1, "int"),
                        "clicshopping_suppliers_id" => new \xmlrpcval($this->suppliers_id, "int"),
                        "ClicShopping_customer_save_to_catalog" => new xmlrpcval(1, "int"),
                        "clicshopping_suppliers_notes" => new \xmlrpcval($this->suppliers_notes, "string"),
                        "category_id" => new \xmlrpcval($labelId, $type_string),
                      );

        $OSCOM_ODOO->createOdoo($values, "res.partner");

      } else {
// update supplier if exist
        $values = array( "ref" => new \xmlrpcval('WebStore Suppliers - ' . $this->suppliers_id, "string"),
                          "name"    => new \xmlrpcval($this->suppliers_name .' ' . $this->suppliers_manager, "string"),
                          "email"  => new \xmlrpcval($this->suppliers_email_address, "string"),
                          'phone' => new \xmlrpcval($this->suppliers_phone,"string"),
                          'fax' => new \xmlrpcval($this->suppliers_fax,"string"),
                          'street' => new \xmlrpcval($this->suppliers_address,"string"),
                          'street2'=>new \xmlrpcval($this->suppliers_suburb,"string"),
                          'zip' => new \xmlrpcval($this->suppliers_postcode,"string"),
                          'city' => new \xmlrpcval($this->suppliers_city,"string"),
                          'country_id' => new \xmlrpcval($this->getCountryIdOdoo(),"int"),
                          "clicshopping_suppliers_id" => new \xmlrpcval($this->suppliers_id, "int"),
                          "ClicShopping_customer_save_to_catalog" => new xmlrpcval(1, "int"),
                          "clicshopping_suppliers_notes" => new \xmlrpcval($this->suppliers_notes, "string"),
                          "category_id" => new \xmlrpcval($labelId, $type_string),
                        );

        $OSCOM_ODOO->updateOdoo($this->getIdOdooSupplier(), $values, 'res.partner');
      }
    }
  }
